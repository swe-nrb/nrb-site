import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable ,  Subscription } from 'rxjs';
import { MatSidenav } from '@angular/material';

@Injectable()
export class SidenavService {
  sidenav: {
    mode: string,
    opened: boolean,
  }
  sub: Subscription
  constructor(
    public breakpointObserver: BreakpointObserver
  ) {
    this.sidenav = {
      mode: 'over',
      opened: false
    }
    this.breakpointObserver.observe([
      Breakpoints.Handset
    ]).subscribe(result => {
      if (result.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.opened = false;
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.opened = true;
      }
    });
  }
  toggle() {
    this.sidenav.opened = !this.sidenav.opened;
  }
  close() {
    this.sidenav.opened = false;
  }
}
