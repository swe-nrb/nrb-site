import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';

@Injectable()
export class ConceptService {
  url: string
  constructor(
    public http: HttpClient
  ) {
    this.url = 'http://storage.infopack.io/swe-nrb/sbp-begreppslista/latest/assets';
  }

  list() {
    return this.http.get<any[]>(`${this.url}/concepts.json`);
  }

  getBySlug(conceptSlug: string): Observable<any> {
    return this.http
      .get<any[]>(`${this.url}/concepts.json`)
      .pipe(
        map((concepts) => _.find(concepts, { slug: conceptSlug }))
      );
  }

  getTags() {
    return this.http.get<any[]>(`${this.url}/tags.json`);
  }

  getSources() {
    return this.http.get<any>(`${this.url}/sources.json`);
  }

  getfileList() {
    return this.http.get<any[]>(`${this.url}/output.json`);
  }

}
