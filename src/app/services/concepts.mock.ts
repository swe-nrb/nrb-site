export const concepts = [{
    "_v": 1,
    "id": "dd7ab6ba-92f3-49f5-c9fc-1dd8d6bdf592",
    "slug": "3d-bildmodell-lantmateriet-ramverk-nationella-geodata-i-3d",
    "filename": "3d-bildmodell-lantmateriet-ramverk-nationella-geodata-i-3d.yml",
    "concept": {
      "sv": {
        "term": "3D-bildmodell",
        "source": "Lantmäteriet",
        "definition": "ytmodell i form av TIN som draperats med bildelement i varje triangel",
        "comment": "Lantmäteriet, Ramverk Nationella geodata i 3D"
      },
      "en": {
        "term": "",
        "source": "",
        "definition": "",
        "comment": ""
      }
    },
    "links": [
      {
        "name": "Lantmäteriet",
        "url": "http://www.lantmateriet.se/globalassets/om-lantmateriet/var-samverkan-med-andra/3d-projektet/ramverk-for-nationella-geodata-i-3d.pdf"
      }
    ]
  },
  {
    "_v": 1,
    "id": "d883a691-5c5c-0cf5-28ef-bdb1e38cdd1e",
    "slug": "3d-lantmateriet-ramverk-nationella-geodata-i-3d",
    "filename": "3d-lantmateriet-ramverk-nationella-geodata-i-3d.yml",
    "concept": {
      "sv": {
        "term": "3D",
        "source": "Lantmäteriet",
        "definition": "Paraplybegrepp inom geodata som omfattar all geodata som upplevs som 3D vid visualisering eller när höjdkomponenten behövs tillsammans med plankoordinater för en 3D-analys - även om resultat i sig visualiseras i 2D. Geodata i 3D behöver inte vara exakta realistiska avbildningar av verkligheten utan kan också nyttjas för olika generaliseringar, detaljeringsgrader, symboler, färgsättningar med mera.",
        "comment": "Lantmäteriet, Ramverk Nationella geodata i 3D"
      },
      "en": {
        "term": "",
        "source": "",
        "definition": "",
        "comment": ""
      }
    },
    "links": [
      {
        "name": "Lantmäteriet",
        "url": "http://www.lantmateriet.se/globalassets/om-lantmateriet/var-samverkan-med-andra/3d-projektet/ramverk-for-nationella-geodata-i-3d.pdf"
      }
    ]
  },
  {
    "_v": 1,
    "id": "a4806eae-abf7-0b53-0427-dad2ee875bfc",
    "slug": "3d-modell-lantmateriets-tekniska-rapport-20164",
    "filename": "3d-modell-lantmateriets-tekniska-rapport-20164.yml",
    "concept": {
      "sv": {
        "term": "3D-modell",
        "source": "Lantmäteriet",
        "definition": "Geodata i 3D (s.k.3D-modeller)–används som ett paraplybegrepp omfattande alla geodata som upplevs som 3D vid visualisering eller när höjdkomponenten behövs tillsammans med plankoordinater för en analys –även om resultat i sig visualiseras i 2D.\nBeträffande geodata handlar det inte om en exakt realistisk avbildning av verkligheten utan olika generaliseringar, detaljeringsgrader m.m. används beroende på ändamålet.",
        "comment": "Lantmäteriets tekniska rapport 2016:4"
      },
      "en": {
        "term": "",
        "source": "",
        "definition": "",
        "comment": ""
      }
    },
    "links": [
      {
        "name": "Lantmäteriet",
        "url": "http://www.lantmateriet.se/globalassets/om-lantmateriet/var-samverkan-med-andra/handbok-mat--och-kartfragor/tekn_rapporter/hmk-tr_2016_4.pdf"
      }
    ]
  },
  {
    "_v": 1,
    "id": "4097e3d4-2e4f-83f2-ea87-9066417649f3",
    "slug": "3d-punktmoln-lantmateriet-ramverk-nationella-geodata-i-3d",
    "filename": "3d-punktmoln-lantmateriet-ramverk-nationella-geodata-i-3d.yml",
    "concept": {
      "sv": {
        "term": "3D-punktmoln",
        "source": "Lantmäteriet",
        "definition": "Färgsatta 3D-punktmoln",
        "comment": "Lantmäteriet, Ramverk Nationella geodata i 3D"
      },
      "en": {
        "term": "",
        "source": "",
        "definition": "",
        "comment": ""
      }
    },
    "links": [
      {
        "name": "Lantmäteriet",
        "url": "http://www.lantmateriet.se/globalassets/om-lantmateriet/var-samverkan-med-andra/3d-projektet/ramverk-for-nationella-geodata-i-3d.pdf"
      }
    ]
  },
  {
    "_v": 1,
    "id": "30884d96-323b-42e4-eb7f-0985edc0b414",
    "slug": "4d-lantmateriet-ramverk-nationella-geodata-i-3d",
    "filename": "4d-lantmateriet-ramverk-nationella-geodata-i-3d.yml",
    "concept": {
      "sv": {
        "term": "4D",
        "source": "Lantmäteriet",
        "definition": "Inom geodataområdet avses tid som den fjärde dimensionen i tillägg till positionen i plan och höjd (3D)",
        "comment": "Lantmäteriet, Ramverk Nationella geodata i 3D"
      },
      "en": {
        "term": "",
        "source": "",
        "definition": "",
        "comment": ""
      }
    },
    "links": [
      {
        "name": "Lantmäteriet",
        "url": "http://www.lantmateriet.se/globalassets/om-lantmateriet/var-samverkan-med-andra/3d-projektet/ramverk-for-nationella-geodata-i-3d.pdf"
      }
    ]
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "61200dd2-0137-4e41-ae4b-2df60d70bf72",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "sv": {
        "term": "absolut koordinatsystem",
        "source": "SS-ISO 16792:2015",
        "source_long": "SS-ISO 16792:2015, Teknisk produktdokumentation – Regler för presentation av produktdefinierande data i 3D miljö, 3.1.1",
        "definition": "primärt modellkoordinatsystem i CAD-modellen använt för att definiera placeringen av digitala element i CAD-modellen",
        "comment": ""
      },
      "en": {
        "term": "absolute coordinate system",
        "source": "ISO 16792:2015",
        "source_long": "ISO 16792:2015 Technical product documentation – Digital product definition data practices, 3.1.1",
        "definition": "primary model coordinate system in the CAD model used to define the location of digital elements in the CAD model",
        "comment": ""
      }
    },
    "slug": "absolut-koordinatsystem-ss-iso-167922015",
    "filename": "absolut-koordinatsystem-ss-iso-167922015.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "fc4cee7e-7970-4b02-b0e9-f33197489b6c",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "abstract",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.6.5",
        "definition": "document (3.2.5) that contains the results of abstracting (3.6.10)",
        "comment": ""
      }
    },
    "slug": "abstract-iso-6707-22017",
    "filename": "abstract-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "67e307e7-edc8-4b49-8356-189b4af8f96e",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "abstract of particulars\r\ngeneral conditions, US\r\nsupplemental general conditions, US",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.4.26",
        "definition": "supplement to the conditions of contract (3.4.25) that provides information on people involved, the period of construction work and maintenance period (3.5.30)",
        "comment": ""
      }
    },
    "slug": "abstract-of-particulars-general-conditions-us-supplementa",
    "filename": "abstract-of-particulars-general-conditions-us-supplementa.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "4bcc1d4b-875e-4709-b2a0-4d67eb2145cc",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "abstracting",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.6.10",
        "definition": "process by which, following taking off (3.6.8), like items are grouped together and arranged in the order in which they will appear in the bill of quantities (3.5.11)",
        "comment": ""
      }
    },
    "slug": "abstracting-iso-6707-22017",
    "filename": "abstracting-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "868156aa-80c4-4057-b95b-56303e2c7c95",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "abutment\r\nbuttress, US",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.3.1.58",
        "definition": "construction (3.3.5.6) intended to resist lateral thrust and vertical load (3.7.3.19) usually from an arch (3.3.1.7) or bridge (3.1.3.19)",
        "comment": "Note 1 to entry: In the US, there is a homograph for the term “buttress”. See 3.3.1.60."
      }
    },
    "slug": "abutment-buttress-us-iso-6707-12017",
    "filename": "abutment-buttress-us-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "5930fceb-27d0-4c15-9105-e2be4a2e4a4c",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "accelerated curing",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.5.1.33",
        "definition": "accelerating rate of gain of strength in concrete (3.4.4.15) or mortar (3.4.4.26) by the application of heat or use of additives (3.4.4.1)",
        "comment": ""
      }
    },
    "slug": "accelerated-curing-iso-6707-12017",
    "filename": "accelerated-curing-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "9ae97199-6ae1-4933-8dd7-742875fd4182",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "accelerator",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.4.4.2",
        "definition": "substance that increases the speed of a chemical reaction",
        "comment": ""
      }
    },
    "slug": "accelerator-iso-6707-12017",
    "filename": "accelerator-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "27e61211-2c18-4711-9790-0d1df0fb054b",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "acceptance",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.5.16",
        "definition": "act of agreeing to a contractor's (3.8.6) offer or tender (3.2.21), thereby creating a binding contract (3.1.1)",
        "comment": ""
      }
    },
    "slug": "acceptance-iso-6707-22017",
    "filename": "acceptance-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "79338517-ace7-4783-9def-6f4768c2d7d0",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "acceptance testing",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.4.5",
        "definition": "testing to establish whether a lot (3.7.4.8) or batch (3.7.4.7) conforms to the specified requirement",
        "comment": ""
      }
    },
    "slug": "acceptance-testing-iso-6707-12017",
    "filename": "acceptance-testing-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "67784a53-9422-48e4-835b-4d775747f6f3",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "sv": {
        "term": "acceptanskriterier",
        "source": "ISO/DIS 19650-2.2:2018",
        "source_long": "ISO/DIS 19650-2.2:2018, Information management using building information modelling –\r Part 2: Delivery phase of the assets, 3.1.1",
        "definition": "begärda bevis med avseende på kravuppfyllnad",
        "comment": "[KÄLLA:  ISO 22263:2008, 2.1]"
      },
      "en": {
        "term": "acceptance criteria",
        "source": "ISO/DIS 19650-2.2:2018",
        "source_long": "ISO/DIS 19650-2.2:2018 Information management using building information modelling –\r Part 2: Delivery phase of the assets, 3.1.1",
        "definition": "evidence required for considering that requirements have been fulfilled",
        "comment": "[SOURCE: ISO 22263:2008, 2.1]"
      }
    },
    "slug": "acceptanskriterier-isodis-19650-222018",
    "filename": "acceptanskriterier-isodis-19650-222018.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "08978d49-d9fb-4a19-85d8-860a60b9fbc4",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "sv": {
        "term": "acceptanskriterier",
        "source": "SS-ISO 22263:2008",
        "source_long": "SS-ISO 22263:2008, Strukturering av information om byggnadsverk – Ramverk för hantering av processinformation (ISO 22263:2008, IDT), 2.1",
        "definition": "begärda bevis med avseende på kravuppfyllnad",
        "comment": ""
      },
      "en": {
        "term": "acceptance criteria",
        "source": "ISO 22263:2008",
        "source_long": "ISO 22263:2008 Organization of information about construction works – Framework for management of project information, 2.1",
        "definition": "evidence required for considering that requirements have been fulfilled",
        "comment": ""
      }
    },
    "slug": "acceptanskriterier-ss-iso-222632008",
    "filename": "acceptanskriterier-ss-iso-222632008.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "2bd35654-35fa-46a2-b670-8520779f0653",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "accepted risk",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.5.57",
        "definition": "risk specified in the contract (3.1.1) for which the client (3.8.2) accepts liability",
        "comment": ""
      }
    },
    "slug": "accepted-risk-iso-6707-22017",
    "filename": "accepted-risk-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "aabc04b5-ecb6-4790-a5a6-6ea25ad74469",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "access balcony\r\nexternal corridor, US",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.2.4.7",
        "definition": "balcony (3.2.2.9) that gives access to a number of units of accommodation",
        "comment": "Note 1 to entry: The units of accommodation can include dwellings (3.1.4.2) or offices (3.2.3.6)."
      }
    },
    "slug": "access-balcony-external-corridor-us-iso-6707-12017",
    "filename": "access-balcony-external-corridor-us-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "f838a22b-4d6e-4cd0-9303-c2883bf67eb6",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "access cover",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.3.4.21",
        "definition": "plate (3.3.5.17), usually hinged to a frame (3.3.1.70) or otherwise capable of being removed, allowing access to a vessel, chamber, gully, pipe (3.3.4.17), or service duct (3.2.4.11)",
        "comment": ""
      }
    },
    "slug": "access-cover-iso-6707-12017",
    "filename": "access-cover-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "be1ac170-5c54-4d5a-a7c7-3311fc047575",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "accidental load",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.3.26",
        "definition": "load (3.7.3.19) that is not specifically foreseen because its occurrence is unlikely but for which an allowance is made in design",
        "comment": ""
      }
    },
    "slug": "accidental-load-iso-6707-12017",
    "filename": "accidental-load-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "da50f132-9960-4611-91e7-f4e27a0a04a0",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "accuracy",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.1.8",
        "definition": "quantitative measure (3.7.1.7) of the degree of conformity with an accepted reference value",
        "comment": ""
      }
    },
    "slug": "accuracy-iso-6707-12017",
    "filename": "accuracy-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "852e9ccb-ab76-4083-b73a-64b95f0aa8b0",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "acidity",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.3.63",
        "definition": "capacity of aqueous media to react with hydroxyl ions",
        "comment": ""
      }
    },
    "slug": "acidity-iso-6707-12017",
    "filename": "acidity-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "9bb02a9f-c161-46f2-ac8d-6955b3d0fb77",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "sv": {
        "term": "ackreditering",
        "source": "EU 305/2011",
        "source_long": "EU 305/2011, EUROPAPARLAMENTETS OCH RÅDETS FÖRORDNING (EU) nr 305/2011\r\nav den 9 mars 2011 om fastställande av harmoniserade villkor för saluföring av byggprodukter och om upphävande av rådets direktiv 89/106/EEG",
        "definition": "den betydelse som anges i förordning (EG) nr 765/2008",
        "comment": ""
      }
    },
    "slug": "ackreditering-eu-3052011",
    "filename": "ackreditering-eu-3052011.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "9ff3cc5e-ebe3-4fe8-9e7c-9696cce71190",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "act of God",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.5.59",
        "definition": "event that results from the operation of natural forces which human foresight cannot be reasonably expected to anticipate",
        "comment": ""
      }
    },
    "slug": "act-of-god-iso-6707-22017",
    "filename": "act-of-god-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "47bba99f-f4e8-4f73-b76a-469ca7f67b43",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "action",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.3.18",
        "definition": "force (3.7.3.22) acting on a structure (3.3.1.2), or cause of deformations (3.7.3.23) imposed on a structure or constrained within it",
        "comment": ""
      }
    },
    "slug": "action-iso-6707-12017",
    "filename": "action-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "47596455-abfc-4400-810b-6622bcca5f30",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "activity space",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.2.3.1",
        "definition": "space (3.2.1.1) defined by the spatial extension of an activity",
        "comment": "Note 1 to entry: A spatial extension of an activity, for example, a table or a bed, and the activity space around them.\r\n[SOURCE: ISO 12006-2:2015, 3.1.9]"
      }
    },
    "slug": "activity-space-iso-6707-12017",
    "filename": "activity-space-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "c5ba98b3-deb6-43f5-a173-223178dbf7ed",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "actor",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.8.1",
        "definition": "person or organizational unit involved in a process or project",
        "comment": "[SOURCE: ISO 29481-1:2016, 3.1, modified – “construction process” was changed to “process or project” and the text in parentheses was deleted.]"
      }
    },
    "slug": "actor-iso-6707-22017",
    "filename": "actor-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "68ad7d79-56a0-493c-8411-a41c9a8e578a",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "actual performance",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.2.27",
        "definition": "achieved performance (3.7.1.1) the value of which is obtained by measurement (3.5.1.22)",
        "comment": "Note 1 to entry: Actual performance can be expressed as actual size, actual position of points, actual position of series of points, actual shape"
      }
    },
    "slug": "actual-performance-iso-6707-12017",
    "filename": "actual-performance-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "b2011fea-6a1b-434d-b71d-0ef42ebfd37b",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "actual size",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.2.4",
        "definition": "size (3.7.2.2) obtained by measurement (3.5.1.22)",
        "comment": "Note 1 to entry: Actual size can be expressed as actual length, actual angle, etc."
      }
    },
    "slug": "actual-size-iso-6707-12017",
    "filename": "actual-size-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "84376866-1b86-411e-938d-da40ae6fae29",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "additive\r\naddition, GB",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.4.4.1",
        "definition": "material (3.4.1.2) added in small quantities to a liquid or granular material to produce some desired modification to its properties (3.7.1.3)",
        "comment": ""
      }
    },
    "slug": "additive-addition-gb-iso-6707-12017",
    "filename": "additive-addition-gb-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "fd7834af-7227-4a61-b971-712b897b2b2e",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "adhesion",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.3.5",
        "definition": "state in which two surfaces are held together by surface bonds",
        "comment": ""
      }
    },
    "slug": "adhesion-iso-6707-12017",
    "filename": "adhesion-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "7b1db22b-0536-4c97-b968-063479fcaa7d",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "adhesive",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.4.4.13",
        "definition": "non-metallic substance capable of joining material (3.4.1.1)",
        "comment": ""
      }
    },
    "slug": "adhesive-iso-6707-12017",
    "filename": "adhesive-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "d539074e-a630-4422-9016-2458ffec148d",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "adit",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.1.2.8",
        "definition": "nearly level tunnel (3.1.3.18) driven to underground workings",
        "comment": ""
      }
    },
    "slug": "adit-iso-6707-12017",
    "filename": "adit-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "id": "ebbb4292-b980-b9f0-e50b-35759ef3dacc",
    "slug": "administrativ-bestammelse-sis",
    "filename": "administrativ-bestammelse-sis.yml",
    "concept": {
      "sv": {
        "term": "Administrativ bestämmelse",
        "source": "SIS",
        "definition": "Planbestämmelse som reglerar administrativa frågor.\n",
        "comment": ""
      },
      "en": {
        "term": "",
        "source": "",
        "definition": "",
        "comment": ""
      }
    },
    "links": [
      {
        "name": "SIS",
        "url": "https://www.sis.se/contentassets/f91a17f266bb49cebb42ee4fc3bc743f/ss-6370402016.pdf"
      }
    ]
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "0c39736f-389c-4497-b330-d766b0f7d41b",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "administrative list of properties\r\nALOP",
        "source": "EN 61987-10:2010",
        "source_long": "EN 61987-10:2010 Industrial-process measurement and control – Data structures and elements in process equipment catalogues – Part 10: Lists of properties (LOPs) for industrial-process measurement and control for electronic data exchange – Fundamentals, 3.1.1",
        "definition": "list of properties describing the aspect concerning initiating, tracking and completing a transaction",
        "comment": "NOTE 1 The administrative list of properties contains, for example, information about the type of document (for example, inquiry, quotation) and the issuing details (for example, contact data of the author) and may be placed at the head of the transaction document.\r\nNOTE 2  An ALOP may apply to a transaction of multiple instances of one or more device types, and will seldom   be related to only a single device type."
      }
    },
    "slug": "administrative-list-of-properties-alop-en-61987-102010",
    "filename": "administrative-list-of-properties-alop-en-61987-102010.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "7ae7fbfb-7844-4b89-a507-30ca70218651",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "admixture",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.4.4.3",
        "definition": "material (3.4.1.2) added in small quantities before or during a mixing process (3.5.2.3) in order to modify the properties (3.7.1.3) of a mixture",
        "comment": ""
      }
    },
    "slug": "admixture-iso-6707-12017",
    "filename": "admixture-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "3e784584-1598-41a6-97ee-3454fb31543e",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "advance payment",
        "source": "ISO 6707-2:2017",
        "source_long": "ISO 6707-2:2017 Buildings and civil engineering works – Vocabulary – Part 2: Contract and communication terms, 3.5.47",
        "definition": "payment made by the client (3.8.2) to a contractor (3.8.6) after the contract (3.1.1) has been signed but before construction work starts or goods or services are supplied",
        "comment": ""
      }
    },
    "slug": "advance-payment-iso-6707-22017",
    "filename": "advance-payment-iso-6707-22017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "8c3db86e-342e-4466-be70-4bf13daf1a25",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "aeration",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.5.1.52",
        "definition": "introduction of air or oxygen",
        "comment": ""
      }
    },
    "slug": "aeration-iso-6707-12017",
    "filename": "aeration-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "5bab5e4b-bcf3-4339-9486-d87d63d25bfd",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "aerial ropeway\r\ncableway, US \r\nlift, US",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.1.3.5",
        "definition": "local transport system for guided passage of cabins or containers carried on cables (3.4.4.54) on intermediate supports",
        "comment": ""
      }
    },
    "slug": "aerial-ropeway-cableway-us-lift-us-iso-6707-12017",
    "filename": "aerial-ropeway-cableway-us-lift-us-iso-6707-12017.yml"
  },
  {
    "_v": 1,
    "_schema": "concept",
    "id": "2b04bfe0-d130-4c8e-96ea-8c7b650e9e70",
    "contributors": [
      {
        "name": "Klas Eckerberg",
        "linkedin": "https://www.linkedin.com/in/klaseckerberg/"
      }
    ],
    "concept": {
      "en": {
        "term": "aerobic action",
        "source": "ISO 6707-1:2017",
        "source_long": "ISO 6707-1:2017 Buildings and civil engineering works – Vocabulary – Part 1: General terms, 3.7.3.53",
        "definition": "biological process (3.5.2.3) in the presence of oxygen",
        "comment": ""
      }
    },
    "slug": "aerobic-action-iso-6707-12017",
    "filename": "aerobic-action-iso-6707-12017.yml"
  }];
