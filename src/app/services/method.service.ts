import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

const METHODS: NrbMethod[] = [
  {
    slug: 'granskning-av-modellbaserat-underlag-bim-alliance',
    name: "Granskning av modellbaserat underlag",
    description: "Syftet med denna rapport är att standardisera granskningsprocessen för objektmodeller och därmed ytterligare effektivisera granskningsprocessen samt kvalitetssäkra informationsleveranser. Klicka på länken nedan för att läsa hela rapporten.",
    edition: "2019-11-18",
    authors: ["BIM Alliance", "Håkan Norberg"],
    links: [{ name: "Granskning av modellbaserat underlag (PDF)", uri: "/assets/docs/methods/granskning-av-modellbaserat-underlag-bim-alliance.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som en fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'granskning-av-handlingar-beast',
    name: "Granskning av handlingar",
    description: "Följande metodik nedan är beskriven enligt BEAst-standard 'BEAST Effektivare granskning' (Version 2.0). Klicka på länken nedan för att läsa mer.",
    edition: "2019-11-18",
    authors: ["BEAst"],
    links: [{ name: "BEAst effektivare granskning (PDF)", uri: "/assets/docs/methods/beast-granskning-av-handlingar.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'referensbeteckning-med-coclass-nrb',
    name: "Referensbeteckning med CoClass",
    description: "Följande metodik beskriver referensbeteckning med CoClass-systemet. Klicka på länken nedan för att läsa mer.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    links: [{ name: "Referensbeteckning med Coclass-systemet (PDF)", uri: "/assets/docs/methods/referensbeteckning-med-coclass-nrb.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'hanvisningar-i-handlingar-beast',
    name: "Hänvisningar i handlingar",
    description: "Följande metodik är beskriven enligt BEAst-standard 'BEAst Hänvisningar'. Klicka på länken nedan för att läsa mer.",
    edition: "2019-11-18",
    authors: ["BEAst"],
    links: [{ name: "BEAST Hänvisningar (PDF)", uri: "/assets/docs/methods/hanvisningar-i-handlingar-beast.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'namnruta-i-handlingar-beast',
    name: "Namnruta i handlingar",
    description: "Följande metodik är beskriven enligt BEAst-standard 'BEAst Namnruta'. Klicka på länken nedan för att läsa mer.",
    edition: "2020-01-30",
    authors: ["BEAst"],
    links: [{ name: "BEAst PDF Guidelines (PDF)", uri: "/assets/docs/methods/namnruta-i-handlingar-beast.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },  
  {
    slug: 'pdf-anvisningar-beast',
    name: "PDF-anvisningar",
    description: "Följande metodik är beskriven enligt BEAst-standard 'BEAst PDF Guidelines'. Klicka på länken nedan för att läsa mer.",
    edition: "2019-11-18",
    authors: ["BEAst"],
    links: [{ name: "BEAst PDF Guidelines (PDF)", uri: "/assets/docs/methods/pdf-anvisningar-beast.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'samverkan-bip-etim-nrb',
    name: "Samverkan BIP och ETIM-koder",
    description: "Följande metodik beskriver möjligheterna att läsa data från ETIM (European Technical Information Model) från produktbeteckningar i BIP (Building Information Properties) för att ge underlag till projektörens och beställarens generella kravställningar på produkter. Klicka på länken nedan för att läsa mer.",
    edition: "2020-01-30",
    authors: ["Nationella Riktlinjer"],
    links: [{ name: "Samverkan BIP och ETIM-koder (PDF)", uri: "/assets/docs/methods/samverkan-bip-etim-nrb.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'uppratta-en-gemensam-datamiljo-nrb',
    name: "Upprätta en gemensam datamiljö (Common Data Environment CDE)",
    description: "För projekt är följande gemensamma datamiljöer aktuella:\n\n* Informationsportal - Information, nyheter och anvisningar gällande det enskilda projektet samlas i en web-baserad informationsportal för att alla medverkande i projektet ska ha tillgång till den senaste versionen.\n* Fildelningsportal - Alla filer i det enskilda projektet distribueras via en fildelningsportal. För fildelningsportaler finns metodik för mappstruktur och filbenämning beskrivet.\n* Modellserver - Projektering i det enskilda projektet kan via en modellserver ske direkt mot servern. Det innebär att alla centralfiler placeras inom modellservern och informationsutbyte sker genom publicering och paketering av modellrelaterad information, till skillnad från traditionell uppladdning av modellfiler.\n* Ärendeplattform - Ärendehantering i det enskilda projektet hanteras i en molnbaserad ärendehanteringsplattform. Alla projektrelaterad kommunikation hanteras och dokumenteras i ärendeplattformen.\n\nVid projektavslut ska samtliga informationsmängder som är nödvändiga för tillgångsförvaltningen flyttas från projektinformationsmodellen (PIM) till tillgångsinformationsmodellen (AIM). Gemensamma datamiljöer för tillgångsförvaltning är under utredning.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'ange-kvalificerare-for-egenskapers-tillamplighet-iec-62596-12017',
    name: "Ange kvalificerare för egenskapers tillämplighet",
    description: "Termen kvalificerare används för att beskriva livscykelsteg för en egenskap, och kan betraktas som metadata för egenskaper för objekt som ingår i en informationsmängd, dock inte som beteckning på hela informationsmängden.\n\nKvalificerare och deras betydelse: Egenskapens tidsstämpel\n* SPE - Som specificerat (as specified): krav från verksamheten i programhandling eller liknande.\n* INQ - Som Efterfrågat (as inquired): tolkning av projektör i förfrågningsunderlag.\n* OFF - Som offererat (as offered): erbjudande från anbudsgivare.\n* CON - Som avtalat (as contracted): avtalat efter förhandling mellan beställare och entreprenör.\n* SUP - Som levererat (as supplied): specifikation av tillverkare (varuägare) eller leverantör.\n* BUILT - Som byggt (as built): redovisat vid överlämnande från entreprenör till beställare.\n* OP - Driftsatt (operated): uppmätt eller konstaterat under driften.\n* DECOM - Avvecklat (decommissioned): redovisning av omhändertagande.\n\nDenna metod blir tillräckligt detaljerad även om man väljer att inte använda alla steg. Metoden för märkningen kan variera och kan göras med hjälp av metadata på en hel leverans, på en fil, eller på individuella egenskaper hos objekten.",
    edition: "2019-11-18",
    authors: ["IEC 62569-1:2017"],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'kvalitetskontroll-egenkontroll-nrb',
    name: "Kvalitetskontroll av informationsleverans - Egenkontroll",
    description: "Inför varje informationsleverans skall utföraren genomföra en erfordelig egenkontroll. Egenkontrollen omfattar såväl informationens sakinnehåll som datastruktur för aktuell leverans. Rutin för egenkontroll ska definieras av utföraren och godkännas av beställaren. Vid behov ska beställaren kunna ta del av utförarens resultat av egenkontroll.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'kvalitetskontroll-samordning-nrb',
    name: "Kvalitetskontroll av informationsleverans - Samordning",
    description: "För att upprätthålla god kvalitet genomförs återkommande samordningar av modellfiler enligt fastställd tidplan under projektets alla skeden. Frekvensen av samordningar bestäms genom samråd mellan informationsansvarig, informationssamordnare, installationssamordnare (om sådan finns) och projekteringsledare.\n\nSamordning och validering av underlag sker på tre nivåer:\n\n* Tvärdisciplinär, visuell granskning\n* Tvärdisciplinära kollisionskontroller\n* Kvalitetskontroll av information i disciplinmodeller och datafiler\n\nÄrenden inom samordning hanteras i ärendeplattform enligt gemensam datamiljö.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'leveransspecifikation-modell-objekt-attribut',
    name: "Leveransspecifikation modell/objekt - Attribut",
    description: "Följande metodik beskriver de attribut som presenteras i leveransspecifikationerna och deras inbördes betydelse, användning etc. Klicka på länken nedan för att läsa mer.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    links: [{ name: "Leveransspecifikation modell/objekt - Attribut (PDF)", uri: "/assets/docs/methods/leveransspecifikation-modell-objekt-attribut-nrb.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'leveransspecifikation-modell-objekt-loin',
    name: "Leveransspecifikation modell/objekt - LOIN (informationsbehov)",
    description: "Följande metodik beskriver de LOIN(informations)-nivåer som presenteras i leveransspecifikationerna och deras inbördes betydelse, användning etc. Klicka på länken nedan för att läsa mer.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    links: [{ name: "Leveransspecifikation modell/objekt - LOIN (PDF)", uri: "/assets/docs/methods/leveransspecifikation-objekt-loin-informationsbehov-nrb.pdf"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'kravhantering-rumsfunktionskrav-nrb',
    name: "Kravhantering - Rumsfunktionskrav",
    description: "Rumsfunktionskrav för vårdbyggnader hanteras i första hand genom Program för Teknisk Standard, PTS. I andra hand genom projektspecifik överenskommelse.",
    edition: "2019-11-18",
    authors: ["Nationella Riktlinjer"],
    links: [{ name: "Program för Teknisk Standard, PTS", uri: "https://www.ptsforum.se/"}],
    language: 'Svenska',
    tags: [
      {
        short_name: "Fastställd",
        full_name: "Fastställd som en nationell riktlinje",
        description: "Indikerar att metoden är kvalificerad som ett fastställd enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers metodlista",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'checklista-kvalitetskontroll-av-rfm-rfa-trafikverket',
    name: "Checklista: Kvalitetskontroll av RFM, RFÄ",
    description: "Den här checklistan är ett stöd i arbetet med kvalitetssäkring av objektorienterade informationsmodeller och utgår från rutinbeskrivningen TDOK 2014:0943 Beställarens kontrollprogram samt instruktionen TDOK 2018:0079 Kvalitetssäkring av objektorienterad informationsmodell – VO PR. \n \n Checklistan används under arbetet med intern kvalitetssäkring av objektorienterade informationsmodeller i projekt. BIM-specialist (BS) och Datasamordnare (DS) kvalitetssäkrar enligt checklistan och vid behov i samråd med teknik- och miljöspecialist (TS/MS). \n \n Kvalitetssäkring av objektorienterade informationsmodeller består av mottagnings-kontroll av leverans och därefter kvalitetskontroll av innehåll.",
    edition: "2019-10-02",
    authors: ["Trafikverket"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'checklista-kvalitetskontroll-av-samordningsmodell-trafikverket',
    name: "Checklista: Kvalitetskontroll av Samordningsmodell",
    description: "Den här checklistan är ett stöd i arbetet med kvalitetssäkring av objektorienterade informationsmodeller och utgår från rutinbeskrivningen TDOK 2014:0943 Beställarens kontrollprogram samt instruktionen TDOK 2018:0079 Kvalitetssäkring av objektorienterad informationsmodell – VO PR. \n \n Checklistan används under arbetet med intern kvalitetssäkring av objektorienterade informationsmodeller i projekt. BIM-specialist (BS) och Datasamordnare (DS) kvalitetssäkrar enligt checklistan och vid behov i samråd med teknik- och miljöspecialist (TS/MS). \n \n Kvalitetssäkring av objektorienterade informationsmodeller består av mottagnings-kontroll av leverans och därefter kvalitetskontroll av innehåll.",
    edition: "2019-10-02",
    authors: ["Trafikverket"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'hantering-byggnad-lantmateriet',
    name: "Hantering Byggnad",
    description: "Geodataspecifikationen för Byggnad är ett dokument som tagits fram i samverkan mellan Sveriges Kommuner och Landsting (SKL), Lantmäteriet och kommunerna. Specifikationen innehåller informationsutbytesmodell och krav på geodata som beskriver Byggnad. Geodataspecifikationen skildrar tillsammans med processbeskrivningen ett framtida scenario av temat Byggnad och ett arbetsflöde där flera aktörer samverkar för att effektivisera samhällsbyggnadsprocessen. Processbeskrivningen som tas fram inom Svensk geoprocess innehåller en redogörelse av hur samverkan kring insamling, lagring och tillhandahållande ska gå till. Geodataspecifikationen avser de geodata om byggnad som Lantmäteriet, kommunerna och andra myndigheter och aktörer t.ex. byggherrar och entreprenörer utbyter information om.",
    edition: "2019-10-02",
    authors: ["Lantmäteriet"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'kommunicera-arenden-med-bcf-nrb',
    name: "Kommunicera ärenden med BCF",
    description: "1. Utför en kontroll på modellen.\n2. Skapa ärenden innehållandes vyer och nödvändig textinformation från resultaten av kontrollen.\n3. Exportera alla ärenden till en rapport i BCF-format.\n\nBCF-filen innehåller lägesinformation, dvs var vypunkten skapades, och den textinformation som angavs på respektive ärende.\n\n4. BCF-filen skickas till aktuell mottagare.\n5. BCF-filen öppnas av mottagaren med en BCF manager.\n\nBCF manager medföljer vissa programvaror eller finns att ladda ned gratis som plug-in eller stand-alone beroende på vilken mjukvara som används av mottagaren.\n\n6. Mottagaren går igenom ärendena i BCF-rapporten och gör nödvändiga justeringar i modellen.\n7. När ett ärende är åtgärdat ändrar mottagaren status på ärendet till ”löst” och kan även lämna kommentarer.\n8. Efter ärendena blivit hanterade exporterar mottagaren en uppdaterad BCF-fil med lösningarna och eventuella kommentarer.\n9. Den uppdaterade BCF-filen och den nya modellen skickas tillbaka till kontrollanten som kontrollerar att ärendena är lösta och att lösningarna fungerar i den uppdaterade modellen.",
    edition: "2019-10-02",
    authors: ["Nationella Riktlinjer"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'matningsanvisningar-byggnad-markdetaljer-markanvandning-och-marktacke-lantmateriet',
    name: "Mätningsanvisningar Byggnad, Markdetaljer, Markanvändning och  Marktäcke",
    description: "Förenklar användningen av specifikationerna Svensk geoprocess och bidrar till att enhetliga geodata kan mätas in på lika sätt oavsett aktör – och enkelt utbytas. Finns för Markdetaljer, Byggnad och Markanvändning och Marktäcke",
    edition: "2019-10-02",
    authors: ["Lantmäteriet"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'matningsanvisningar-byggnad-markdetaljer-markanvandning-och-marktacke-lantmateriet',
    name: "Mätningsanvisningar Byggnad, Markdetaljer, Markanvändning och  Marktäcke",
    description: "Förenklar användningen av specifikationerna Svensk geoprocess och bidrar till att enhetliga geodata kan mätas in på lika sätt oavsett aktör – och enkelt utbytas. Finns för Markdetaljer, Byggnad och Markanvändning och Marktäcke",
    edition: "2019-10-02",
    authors: ["Lantmäteriet"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'ta-fram-sammansatt-3d-modell-nrb',
    name: "Ta fram sammansatt 3D-modell",
    description: "Lägg alla modeller som ska sammanställas lokalt på datorn. Öppna ett samordningsprogram och importera modellerna. Spara den sammansatta modellen. Om uppdateringar av modellen ska göras ersätts de gamla modellfilerna, som sparats lokalt, mot de nya, uppdaterade, modellerna. Välj sedan att uppdatera den sammansatta modellen i samordningsprogrammet istället för att skapa nya sammanställningar.",
    edition: "2019-10-02",
    authors: ["Nationella Riktlinjer"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  },
  {
    slug: 'overforing-av-ifc-modell-mellan-tva-cadsystem-nrb',
    name: "Överföring av IFC modell mellan två (CAD)system",
    description: "Denna metod används för att överföra objektsorienterad information (objekt och data) mellan olika CAD system med avsikt att öka transparensen mellan ingående aktörer i olika skeden av ett byggprojekt. IFC-filer utbyts genom applikationer som använder följande format, vilka också indikeras av ikonerna:\n\n**.ifc**\n\nIFC-datafil som använder STEP-strukturen (Standard for the Exchange of Product) enligt ISO 10303-21.\nDetta är standardformatet vid utbyte av IFC-filer.\n\n**.ifcXML**\n\nIFC-datafil som använder XML-dokumentsstrukturen. Den kan genereras direkt från den avsändande applikationen eller från en IFC-datafil genom konvertering som följer ISO 10303-28. En .ifcXML fil är normalt 300-400 % större än en .ifc fil.\n\n**.ifcZIP**\n\nIFC-datafil som använder PKzip 2.04g kompressionsalgoritmen (kompatibel med t ex Windows komprimerade mappar, winzip, zlib, info-zip, etc). Den kräver en ensam .ifc eller .ifcXML datafil i huvudmappen av zip-arkivet. En .ifcZIP fil komprimerarnormalt ned en .ifc  med 60-80 % och en ifcXML fil med 90-95 %.\n\nFör en lyckad IFC-export är det möjligt att göra vissa exportinställningar i CAD-applikationen. Detta kan exempelvis vara inställningar som filtrerar bort objekt och information som inte efterfrågas av mottagaren av exportfilen. Export av IFC-filer ska även följa projektets leveransspecifikation om en sådan finns upprättad. Exempel på krav i leveransspecifikationen är:\n\n* Vilken IFC-version och format som exporten ska göras i\n* Vilken information modellen ska innehålla (vanligen kravställt i LOD-matris)\n* Vilken grad av detaljnivå objekten ska ha (för hög detaljeringsgrad leder till onödigt tunga modeller)\n\nI CAD-applikationer är det möjligt att göra permanenta exportinställningar. Exempel på inställningar är:\n\n*	Filtrera bort objekt som aktuell part inte ansvarar för\n* Filtrera bort skissobjekt, icke gällande objekt etc\n* Dölj enskilda objekt som uppenbarligen inte ska vara med i export\n\nInnehållet i den exporterade IFC-filen kontrolleras innan leverans. Exempel på kontroller är:\n\n*	Kontrollera att modellen är exporterad med rätt koordinatsystem\n* Kontrollera att modellen inte innehåller objekt utanför byggnaden\n* Kontrollera att rätt objekt medföljt exporten\n\nEfter egenkontroll levereras IFC-modellen enligt projektets leveransspecifikationen (via projektportal etc) till BIM-samordnaren.",
    edition: "2019-10-02",
    authors: ["Nationella Riktlinjer"],
    language: 'Svenska',
    tags: [
      {
        short_name: "För information",
        full_name: "För information",
        description: "Sidoinformation av intresse",
        status: "complete",
        percentDone: 1
      }
    ]
  }
];

@Injectable()
export class MethodService {
  url: string
  constructor(
    public http: HttpClient
  ) {
    this.url = 'https://swe-nrb.gitlab.io/sbp-metoder/assets';
  }

  /**
   * TODO: Configure site to update, see https://gitlab.com/swe-nrb/nrb-site/issues/114
   * Disabled temporary
   */
  /*
  list() {
    return this.http.get<any[]>(`${this.url}/collection.json`);
  }
  */

  list(): Observable<NrbMethod[]> {
    return of(METHODS);
  }

  getBySlug(slug: string): Observable<any> {
    return of(METHODS)
      .pipe(
        map((methods) => _.find(methods, { slug: slug }))
      );
  }

  getfileList() {
    return this.http.get<any[]>(`${this.url}/output.json`);
  }

}

export interface NrbMethod {
  slug: string,
  name: string,
  description: string,
  /**
   * Swe: Utgåva
   */
  edition: string,
  authors: string[],
  publisher?: string,
  published?: number,
  language?: string,
  links?: {
    name: string,
    uri: string
  }[],
  imageUrl?: string
  tags?: {
    short_name: string,
    full_name: string,
    description: string,
    status: string,
    percentDone: number
  }[]

}