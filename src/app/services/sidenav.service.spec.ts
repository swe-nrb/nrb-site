import { TestBed, inject } from '@angular/core/testing';

import { SidenavService } from './sidenav.service';
import { BreakpointObserver, MediaMatcher } from '@angular/cdk/layout';
import { Platform } from '@angular/cdk/platform';

describe('SidenavService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SidenavService,
        BreakpointObserver,
        MediaMatcher,
        Platform
      ]
    });
  });

  it('should be created', inject([SidenavService], (service: SidenavService) => {
    expect(service).toBeTruthy();
  }));
});
