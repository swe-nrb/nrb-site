import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ProcessService } from './process.service';

describe('ProcessService', () => {
  let service: ProcessService
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [ProcessService]
    });

    service = TestBed.get(ProcessService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([ProcessService], (service: ProcessService) => {
    expect(service).toBeTruthy();
  }));
});
