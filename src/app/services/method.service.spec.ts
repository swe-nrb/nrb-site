import { TestBed, inject } from '@angular/core/testing';

import { MethodService } from './method.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('MethodService', () => {
  let service: MethodService
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [MethodService]
    });

    service = TestBed.get(MethodService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([MethodService], (service: MethodService) => {
    expect(service).toBeTruthy();
  }));
});
