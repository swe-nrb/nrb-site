import { first, mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable()
export class ProcessService {

  constructor(
    private http: HttpClient
  ) { }

  list() {
    return of([
      {
        phase: "Idéskede",
        code: "0",
        purpose: "Initiativ",
        definition: "process där behov framträder och fastställs",
        activities: [],
        exampleResults: [],
        purposeEng: "Inititative",
        definitionEng: "process where needs are brought forward and documented"
      },
      {
        phase: "Marknadsanalys",
        code: "0.1",
        purpose: "Marknadsundersökning",
        definition: "del av initiativ där (framtida) behov av en byggd tillgång identifieras",
        activities: [],
        exampleResults: ["marknadsanalys", "behovsanalys", "strategisk orientering"],
        purposeEng: "",
        definitionEng: ""
      },
      {
        phase: "Affärsanalys",
        code: "0.2",
        purpose: "Affärsanalys",
        definition: "del av initiativ där ett strukturerat beslutsunderlag utarbetas, med risker och framgångsfaktorer identifierade",
        activities: [],
        exampleResults: ["kostnadsöverslag", "lönsamhetskalkyl", "finansieringsanalys"],
        purposeEng: "",
        definitionEng: ""
      },
      {
        phase: "Planeringsskede/Programskede",
        code: "1",
        purpose: "planering",
        definition: "process som syftar till att inventera behov och förutsättningar och formulera krav för ett byggresultat",
        activities: ["utredning", "kalkylering", "miljöplanering", "riskhantering", "tidsplanering"],
        exampleResults: [],
        purposeEng: "Planning",
        definitionEng: "process where needs and prerequisites are inventoried, and requirements are formulated"
      },
      {
        phase: "Projektstart",
        code: "1.1",
        purpose: "Projektstart",
        definition: "del av planering där den övergripande omfattningen definieras och behoven analyseras",
        activities: [],
        exampleResults: ["verksamhetsbeskrivning"],
        purposeEng: "",
        definitionEng: ""
      },
      {
        phase: "Förstudie",
        code: "1.2",
        purpose: "Förstudie",
        definition: "del av planering där möjligheterna att tillfredsställa behoven undersöks",
        activities: [],
        exampleResults: ["åtgärdsvalsstudie", "genomförbarhetsstudie", "miljökonsekvensbeskrivning"],
        purposeEng: "",
        definitionEng: ""
      },
      {
        phase: "Programskede",
        code: "1.3",
        purpose: "Programskrivning",
        definition: "del av planering där ambitioner, krav, önskemål, förväntningar, begränsningar och myndighetskrav identifieras, analyseras och dokumenteras",
        activities: [],
        exampleResults: ["programhandling", "utrymmesprogram", "byggnadsprogram", "rumsfunktionsprogram", "vägutredning", "järvägsutredning"],
        purposeEng: "",
        definitionEng: ""
      }
    ]);
  }

}
