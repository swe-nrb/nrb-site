import { TestBed, inject } from '@angular/core/testing';

import { ConceptService } from './concept.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { concepts } from './concepts.mock';

describe('ConceptService', () => {
  let service: ConceptService
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [ConceptService]
    });

    service = TestBed.get(ConceptService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([ConceptService], (service: ConceptService) => {
    expect(service).toBeTruthy();
  }));

  it('should retrieve concepts from the API', () => {


    service.list().subscribe((concepts) => {
      expect(concepts.length).toBe(concepts.length);
    });

    const request = httpMock.expectOne(`${service.url}/collection.json`);

    expect(request.request.method).toBe('GET');

    request.flush(concepts);
  });
});
