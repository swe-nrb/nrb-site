import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReferenceService {
  constructor() { }
  list(): Observable<NrbReference[]> {
    return of([
      {
        name: "CoClass - Informationshantering för byggd miljö",
        description: "Boken innehåller en grundlig genomgång av klassifikationsteori och hur den tillämpas. En beskrivning av den historiska utvecklingen av bygg­klassifikation ingår, liksom jämförelser med internationella system. CoClass  - Informationshantering för byggd miljö riktar sig till alla kategorier i bygg- och fastighetssektorn som vill skaffa sig information och djupare kunskap om digital informations­hantering för byggd miljö.",
        type: "Handbok",
        edition: "1",
        language: "Svenska",
        authors: ["Klas Eckerberg"],
        publisher: "Svensk Byggtjänst",
        published: 2019,
        imageUrl: "https://byggtjanst.se/imagefiles/9639d5ee99ed4570a0b26e83d8f2248b/a10f06d0068849da8c6970461d1afa91.jpg",
        links: [{
          name: "Svensk Byggtjänst",
          uri: "https://byggtjanst.se/bokhandel/kategorier/projektering-upphandling/program-projektering-beskrivning/boken-om-coclass/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "CoClass - nya generationen BSAB",
        description: "CoClass är det nya svenska klassifikationssystemet för all byggd miljö. CoClass förbättrar kommunikationen mellan samhällsbyggnadssektorns aktörer. Syftet med CoClass är att begreppsmodellen ska användas av alla parter under hela byggnadens livscykel - från tidiga skeden till förvaltning och rivning. Med CoClass får alla parter tillgång till ett gemensamt språk med samma begrepp och terminologi i alla programvaror och i alla informationsleveranser.",
        type: "Webbtjänst",
        edition: "1",
        language: "Svenska",
        authors: ["Klas Eckerberg"],
        publisher: "Svensk Byggtjänst",
        published: 2019,
        imageUrl: "https://byggtjanst.se/globalassets/tjanster/bsab/bsabcoclass/pil_coclass_ton.jpg",
        links: [{
          name: "Svensk Byggtjänst",
          uri: "https://coclass.byggtjanst.se/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "SB Rekommendationer 11 CAD-lager med CoClass",
        description: "CAD-lager med CoClass utgör utgåva 4 av SB 11 CAD-lager, och är en rekommendation med utförliga anvisningar för hur lager i CAD-filer ska benämnas. Boken innehåller en beskrivning av det klassifikationssystemet CoClass. I CAD-lager med CoClass beskrivs lagernamnets alla fält, med värdelistor för de som vanligen används. Boken ger anvisningar för indelning av CAD-lager för alla typer av handlingar. Den vänder sig till dig inom planering, projektering, byggproduktion och förvaltning som använder CAD-program eller som använder information från CAD-filer.",
        type: "Handbok",
        edition: "4",
        language: "Svenska",
        authors: ["Klas Eckerberg"],
        publisher: "Svensk Byggtjänst",
        published: 2017,
        imageUrl: "http://resources.mynewsdesk.com/image/upload/rixupzuzrllal4izza6m.jpg",
        links: [{
          name: "Svensk Byggtjänst",
          uri: "https://byggtjanst.se/bokhandel/kategorier/projektering-upphandling/program-projektering-beskrivning/cad-lager-med-coclass/"
        }],
        tags: [{
          short_name: "För information",
          full_name: "För information",
          description: "Sidoinformation av intresse",
          status: "complete",
          percentDone: 1
        }]
      },
      {
        name: "Bygghandlingar 90 del 8 - Digitala leveranser för bygg och förvaltning",
        description: "Bygghandlingar 90 innehåller rekommendationer som grundar sig främst på dokumentbaserad och manuell informationshantering, men som är till stor del även relevanta för modellorienterad och automatiserad informationshantering. \n \n Där relevant och möjligt utgår Riktlinjerna från Bygghandlingar 90 med syfte att ta fasta på väletablerade koncept såsom informationssamordning, filhantering, kvalitet och ändringar, informationsmängder och leveransspecifikationer. Bygghandlingar 90 del 8 är avsedd att användas i arbetet med att definiera och genomföra utväxling av information, informationsleveranser; på så sätt utgör den en bas för att specificera information och rutiner. \n \n Avsikten är att komplettera den beskrivning av redovisningsformer och redovisningssätt som behandlas i delarna 1-7 med den underliggande hanteringen av data, för att underlätta ett obrutet informationsflöde mellan olika skeden och mellan olika aktörer, från projektör till slutanvändare.",
        type: "Handbok",
        edition: "2",
        language: "Svenska",
        authors: ["Kurt Löwnertz"],
        publisher: "SIS Förlag AB",
        published: 2008,
        imageUrl: "https://byggtjanst.se/imagefiles/3b9e286eab1241b885c9ad0248a657a3/d966c7e2ef764c1db065d2abc5b32227.jpg",
        links: [{
          name: "Svensk Byggtjänst",
          uri: "https://www.sis.se/bcker/bygghandlingar90del8digitalaleveranserfrbyggochfrvaltning/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Bygghandlingar 90 del 7 - Redovisning av anläggning",
        description: "Boken innehåller en grundlig genomgång av klassifikationsteori och hur den tillämpas. En beskrivning av den historiska utvecklingen av bygg¬klassifikation ingår, liksom jämförelser med internationella system. CoClass  - Informationshantering för byggd miljö riktar sig till alla kategorier i bygg- och fastighetssektorn som vill skaffa sig information och djupare kunskap om digital informations¬hantering för byggd miljö.",
        type: "Handbok",
        edition: "2",
        language: "Svenska",
        authors: ["Klas Eckerberg"],
        publisher: "SIS Förlag AB",
        published: 2011,
        imageUrl: "https://www.sis.se/globalassets/bckerochverktyg/bilder/bokomslag/bck80448png",
        links: [{
          name: "Svensk Byggtjänst",
          uri: "https://byggtjanst.se/bokhandel/kategorier/projektering-upphandling/program-projektering-beskrivning/Bygghandlingar-90-del-7-Redovisning-av-anlaggning/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "SS-EN ISO 19650-1:2019 Strukturering av information om byggd miljö - Informationshantering genom byggnadsinformationsmodellering - Del 1: Begrepp och principer (ISO 19650-1:2018)",
        description: "Boken innehåller en grundlig genomgång av klassifikationsteori och hur den tillämpas. En beskrivning av den historiska utvecklingen av bygg¬klassifikation ingår, liksom jämförelser med internationella system. CoClass  - Informationshantering för byggd miljö riktar sig till alla kategorier i bygg- och fastighetssektorn som vill skaffa sig information och djupare kunskap om digital informations¬hantering för byggd miljö.",
        type: "Standard",
        edition: "1",
        language: "Engelska",
        authors: ["SIS"],
        publisher: "ISO",
        published: 2019,
        imageUrl: "https://www.sis.se/globalassets/logotyper/sis_logotype_sv_lockup_red_black_animated-1.svg",
        links: [{
          name: "SIS – Svenska Institutet för Standarder",
          uri: "https://www.sis.se/produkter/informationsteknik-kontorsutrustning/ittillampningar/ittillampningar-inom-bygg-och-anlaggningsindustri/ss-en-iso-19650-12019/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "SS-EN ISO 19650-1:2019 Strukturering av information om byggd miljö - Informationshantering genom byggnadsinformationsmodellering - Del 2: Informationsleverans vid överlämning av tillgångar (ISO 19650-2:2018)",
        description: "Detta dokument är utformat för att göra det möjligt för en kravställande part att fastställa sina informationsbehov under leveransfasen av tillgångar och att tillhandahålla rätt arbetsmiljö som främjar samarbete, inom vilka (flera) utförare kan producera information på ett effektivt sätt. Detta dokument kan tillämpas på alla typer av tillgångar och organisationsstorlekar, oavsett vald upphandlingsform.",
        type: "Standard",
        edition: "1",
        language: "Engelska",
        authors: ["SIS"],
        publisher: "ISO",
        published: 2019,
        imageUrl: "https://www.sis.se/globalassets/logotyper/sis_logotype_sv_lockup_red_black_animated-1.svg",
        links: [{
          name: "SIS – Svenska Institutet för Standarder",
          uri: "https://www.sis.se/produkter/informationsteknik-kontorsutrustning/ittillampningar/ittillampningar-inom-bygg-och-anlaggningsindustri/ss-en-iso-19650-12019/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Leda byggprojekt",
        description: "Leda byggprojekt är ett hjälpmedel för att underlätta bestämningen av konsultuppdragets innehåll, kvalitet och omfattning vid upphandling av projektledningstjänster för såväl privata som offentliga beställare. Boken kan utgöra en bas till upprättande av en projektspecifikation. Skedesindelingen och informationsleveranser i boken är baserade på traditionella handlingar. Tillämpning av boken i en informationsmodell baserat praxis är möjlig, men skedesindelingen och informationsleveranser behöver anpassas.",
        type: "Handbok",
        edition: "2",
        language: "Svenska",
        authors: ["Jan-Erik Ivansson"],
        publisher: "Svensk Byggtjänst",
        published: 2018,
        imageUrl: "https://byggtjanst.se/imagefiles/51d13e7ca4a6442aa0e45cf8ca85c585/8f848e51218a4e9682b2b934e7fb03da.jpg",
        links: [{
          name: "Svensk Byggtjänst",
          uri: "https://byggtjanst.se/bokhandel/kategorier/projektering-upphandling/leda-byggprojekt.-utg-2/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BEAst Document – Överföring av bygghandlingar och metadata",
        description: "I ett SBUF-finansierat utvecklingsprojekt har en arbetsgrupp tagit fram branschstandarden BEAst Document. Den skapar möjligheter för att digitalt och effektivt överföra handlingar tillsammans med uppgifter om handlingen – metadata – mellan olika företag och deras dokumentplattformar. Det kan gälla både ritningar och andra slags dokument som används i byggprocessen och som skickas mellan byggherrar, projektörer, entreprenörer och leverantörer under upphandling och produktion. \n \n Syftet med standarden BEAst Document är att minska dagens omfattande manuella hanteringen samt att öka kvaliteteten, t.ex. för att eliminera risken med att arbeta enligt fel version av en handling. Dessa positiva effekter har påvisats i slutrapporten från projektet.",
        type: "Standard",
        edition: "2",
        language: "Svenska",
        authors: ["BEAst"],
        publisher: "BEAst",
        published: 2018,
        imageUrl: "https://beast.se/wp-content/themes/beastab/graphics/logotypeBeast.gif",
        links: [{
          name: "BEAst",
          uri: "https://beast.se/standarder/beast-document/beast-document-api/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BEAst Standard för effektivare granskning",
        description: "År 2017 tog byggbranschen tagit fram en standard för att förenkla hanteringen vid granskning av system- och bygghandlingar, t.ex. ritningar. Standarden är framtagen i ett SBUF-projekt av en arbetsgrupp i BEAst. Målet är att förenkla den omfattande och ofta manuella hanteringen i granskningsprocessen genom att digitalisera och standardisera. \n \n En del företag har skaffat systemstöd för granskning, vilket är ett viktigt steg, men för att få ut den potentiella nyttan med digitalisering behövs en standard så att branschen kan arbeta enhetligt. Det är särskilt viktigt i byggbranschen där företag samverkar i olika konstellationer för varje nytt projekt. \n \n BEAst har även tagit fram PDF Guidelines med exempel som stöd vid PDF-export för att underlätta rätt teknisk innehållsmässigt inför leverans av handlingar. Det är en förutsättning för snabb åtkomst och sökbarhet samt skapa automatgenererade hyperlänkar. Underlättar att skapa automatisk hyperlänkar för att även se vilka handlingar som saknar korrekta hänvisningar. Målgruppen är t.ex. entreprenörer, installatörer, byggherrar, konsulter och arkitekter.",
        type: "Standard",
        edition: "2",
        language: "Svenska",
        authors: ["BEAst"],
        publisher: "BEAst",
        published: 2017,
        imageUrl: "https://beast.se/wp-content/themes/beastab/graphics/logotypeBeast.gif",
        links: [{
          name: "BEAst",
          uri: "https://beast.se/standarder/beast-document/beast-document-api/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BIM i staten",
        description: "BIM i staten är ett initiativ från de fem statliga byggherrarna – Specialfastigheter, Akademiska hus, Riksdagsförvaltningen, Statens Fastighetsverk och Fortifikationsverket. \n \n Projektet har tagit fram en gemensam strategi samt riktlinjer för arbete och användande av byggnadsinformationsmodeller och informationshantering i projekt och förvaltning. Strategin innebär bland annat att organisationerna strävar mot öppna neutrala standarder. Vidare identifierar strategin ett antal prioriterade områden där fortsatt utveckling ska ske. Det utvecklingsarbetet kommer till stor del att ske i form av pilotprojekt inom respektive organisation. Strategin kompletteras av riktlinjer för hantering av BIM i projekt samt för informationshantering i förvaltning.",
        type: "Rapport",
        edition: "2",
        language: "Svenska",
        authors: ["BIM Alliance"],
        publisher: "BIM Alliance",
        published: 2017,
        imageUrl: "https://www.bimalliance.se/library/4885/bim-cmyk.png",
        links: [{
          name: "BIM Alliance",
          uri: "https://www.bimalliance.se/utveckling-av-bim/initiativ-i-sektorn/bim-i-staten/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Virtuell produktionsplanering - BIM för installatörer",
        description: "SBUF-projektet är ett projekt vars slutredovisning urgörs av rapporten 'Virtuell produktionsplanering - BIM för installatörer', med målet att skapa ett informationsmaterial för installatörer i form av en skrift med tillhörande webbfilmer och kortfattade råd och anvisningar för projekteringsledning och installatörernas personal.",
        type: "Rapport",
        edition: "1",
        language: "Svenska",
        authors: ["SBUF"],
        publisher: "SBUF",
        published: 2012,
        imageUrl: "https://www.sbuf.se/globalassets/logo-sbuf/sbuf_logo_black_lge_v.2.jpg",
        links: [{
          name: "SBUF - BIM för installatörer",
          uri: "https://www.sbuf.se/Projektsida/?id=09d5a50e-910e-468f-adcd-4921fa258e45",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Avtalsmallar digitala leveranser",
        description: "Trafikverket har i samverkan med branschens aktörer (bland annat genom SBUF och STD) tagit fram riktlinjer för juridiken kring digitala leveranser i projektering och entreprenader för såväl hus- som anläggningsbyggande. \n Formen för detta är två villkorsbilagor att bifoga till uppdragskontrakt enligt ABK 09 respektive AB 04. I villkorsbilagorna kan parterna reglera bl a nyttjanderätt av och ansvar för den digitala informationen samt även kunna ge denna en juridisk status att likställas med beskrivningar enligt kontraktshandlingarna.",
        type: "Mall",
        edition: "1",
        language: "Svenska",
        authors: ["BIM Alliance"],
        publisher: "BIM Alliance",
        published: 2013,
        imageUrl: "https://www.bimalliance.se/library/4885/bim-cmyk.png",
        links: [{
          name: "BIM Alliance",
          uri: "https://www.bimalliance.se/verktyg-och-stoed/hjaelpmedel-och-produktstoed/avtalsmallar/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Region Dalarna – Digitala anvisningar",
        description: "Denna informationsportal innehåller krav och riktlinjer för digital informationshantering med hjälp av objektorienterade informationsmängder och syftar till att skapa ett strukturerat och enhetligt arbetssätt inom Region Dalarnas projekt.",
        type: "Webbplats",
        edition: "1",
        language: "Svenska",
        authors: ["-"],
        publisher: "Region Dalarna",
        published: 2013,
        imageUrl: "http://resources.mynewsdesk.com/image/upload/t_next_gen_logo_limit_x2_png/lxnyd9otko4niwvjgyck.png",
        links: [{
          name: "Region Dalarna",
          uri: "http://regiondalarna.bimportal.se/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Penn State BIM Project Execution Planning Guide",
        description: "För att på ett bra sätt implementera ett BIM-baserat arbetssätt behöver projektgruppen utföra en detaljerad och omfattande planering. En väl dokumenterad s.k. BIM-genomförandeplan säkerställer att alla parter är tydligt medvetna om de möjligheter och ansvarsområden förknippade med införlivandet av BIM i projektets arbetsflöde. En god BIM-genomförandeplan ska definiera lämpliga BIM-användningar på ett projekt (exempelvis granskning i projektering), tillsammans med en detaljerad dokumentation av processen för att genomföra digitala arbetssätt under hela byggnaden eller anläggningens livscykel. När BIM-genomförandeplanen är färdigställd kan teamet följa och övervaka sin progression mot denna plan för att få maximal nytta av införlivandet av ett digitalt arbetssätt. \n \n Denna handbok tillhandahåller en strukturerad och stegvis arbetsgång för både framtagandet och implementingen av en BIM-genomförandeplan. Notera att Kapitel 5 är särskilt intressant.",
        type: "Handbok",
        edition: "2.2",
        language: "Engelska",
        authors: ["Penn State"],
        publisher: "Penn State",
        published: 2019,
        imageUrl: "https://psu.pb.unizin.org/app/uploads/sites/189/2019/01/TITLE_PAGE_V2.2.png",
        links: [{
          name: "BIM Project Execution Planning Guide – Version 2.2",
          uri: "https://psu.pb.unizin.org/bimprojectexecutionplanningv2x2/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Statsbygg BIM-manual 1.2.1 (Norge)",
        description: "SBM1.2.1 innehåller Statsbyggs allmänna krav för byggnadsinformationsmodellering (BIM) i projekt och för fastigheter. Manualen är baserad på tidigare versioner 1.0, 1.1 och 1.2 i manualen och på erfarenheter som Statsbygg har fått genom sina byggprojekt och FoU-projekt. Syftet med SBM är att beskriva Statsbyggs krav för att bygga byggnadsinformationsmodeller (BIM) i öppna format - både allmänna krav och krav relaterade till specifika BIM-syften. Kraven kan kompletteras eller ändras under genomförandet av projekt. \n \n De viktigaste målgrupperna för SBM är främst teknikkonsulter, projektledning och beställare, samt andra konsultgrupper involverade i BIM-processen. Det kan också vara relevant som vägledning för mjukvaruförsäljaren.",
        type: "Standard",
        edition: "1.2.1",
        language: "Engelska, Norska",
        authors: ["Statsbygg"],
        publisher: "Statsbygg",
        published: 2013,
        imageUrl: "https://imgv2-1-f.scribdassets.com/img/document/369310379/298x396/6a9df9e04d/1516149323?v=1",
        links: [{
          name: "Statsbygg",
          uri: "https://www.statsbygg.no/files/publikasjoner/manualer/StatsbyggBIM-manual-Ver1-2-1-2013-12-17.pdf",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Projekt DigSam",
        description: "Det strategiska projektet DigSam arbetar för att skapa ett obrutet digitalt standardiserat informationsflöde med särskilt fokus på processens tidiga planeringsskeden — att genomföra översiktsplanering, medborgardialog och medskapande med användning av digitaliseringens tekniska möjligheter. Därutöver kommer projektet att kartlägga möjligheterna till enklare lagring och åtkomst av data samt vilka upplevda juridiska hinder som finns för en digital samhällsbyggnadsprocess. \n \n DigSam är en utökning och fördjupning av projektet ”Smart planering för byggande” inom Smart Built Environment och ett resultat av regeringens samverkansprogram ”Smarta Städer”.",
        type: "Handbok",
        edition: "1",
        language: "Svenska",
        authors: ["Smart Built Environment"],
        publisher: "Smart Built Environment",
        published: 2019,
        imageUrl: "https://www.smartbuilt.se/library/4887/logo-ny-beskard.png",
        links: [{
          name: "Smart Built Environment - Projekt DigSam",
          uri: "https://www.smartbuilt.se/projekt/informationsinfrastruktur/digsam/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BEAst Standard för hänvisningar i handlingar",
        description: "BEAst har tagit fram guider och anvisningar med exempel för hänvisningar mellan handlingar för att förenkla hanteringen med snabbare åtkomst till dokument och mellan dokument samt enklare se om något dokument saknas till en hänvisning, allt blir i princip klickbart för att komma till respektive handling. Målet är att förenkla åtkomst och ge snabbare till handlingar som ritningar och uppställningsritningar samt mellan hänvisningar i handlingar. Genom att ha gemensamma anvisningar med exempel underlättar det att digitalisera och standardisera vid leverans av handlingar.",
        type: "Standard",
        edition: "1",
        language: "Svenska",
        authors: ["BEAst"],
        publisher: "BEAst",
        published: 2017,
        imageUrl: "https://beast.se/wp-content/themes/beastab/graphics/logotypeBeast.gif",
        links: [{
          name: "BEAst",
          uri: "https://beast.se/standarder/beast-document/beast-hanvisningar/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BEAst Supply",
        description: "Standarden inkluderar hela byggprocessen med fokus på planering, avrop och leverans. Med standardens informationsflöde kan man kontinuerligt uppdatera varandra och även en logistiskt komplex bransch som byggsektorn kan ta steget in i effektiv 'supply chain management'. Standarden BEAst Supply Material består av en processbeskrivning och en serie meddelanden som ska användas för leveranser av byggvaror och annat material. Det kan vara till en byggarbetsplats eller till en grossist eller detaljist. Som ett komplement till BEAst Supply finns BEAst Label som är en standardiserad kollietikett. \n \n BEAst Supply består av tre standarder som stöttar var sitt flöde: \n • BEAst Supply Material ska användas för varuförsörjning till byggarbetsplatser. \n • BEAst Supply NeC ska användas för anläggningstransporter och maskintjänster. \n • BEAst Supply Rental för hyresprocessen.",
        type: "Standard",
        edition: "1",
        language: "Svenska",
        authors: ["BEAst"],
        publisher: "BEAst",
        published: 2017,
        imageUrl: "https://beast.se/wp-content/themes/beastab/graphics/logotypeBeast.gif",
        links: [{
          name: "BEAst",
          uri: "https://www.beast.se/standarder/beast-supply-material",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BEAst Label",
        description: "Standarden ska användas för leveranser till byggarbetsplatser och har som syfte att effektivisera den interna logistiken på en byggarbetsplats. Det primära syftet med BEAst Label är identifiering, godsmottagning och intern styrning av gods och material på en byggarbetsplats men också hos 3PL eller t.ex. grossister och återförsäljare. \n \n Användning och tillämpning av BEAst Label är i första hand avsedd för, men inte begränsat till, märkning av gods och material inom bygg- och installationsbranschen, men är fri att användas även inom andra branscher och tillämpningar. BEAst Label har tagits fram inom ramarna för projektet Effektivare Varuförsörjning inom BEAst, ett projekt som finansierats av svenska byggbranschens utvecklingsfond, SBUF.",
        type: "Standard",
        edition: "1",
        language: "Svenska",
        authors: ["BEAst"],
        publisher: "BEAst",
        published: 2017,
        imageUrl: "https://beast.se/wp-content/themes/beastab/graphics/logotypeBeast.gif",
        links: [{
          name: "BEAst Label",
          uri: "https://www.beast.se/standarder/beast-label/",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Granskning av modellbaserat underlag",
        description: "Under 2017 genomförde medlemmar inom BEAst (Byggbranschens Elektroniska Affärsstöd) utvecklingsprojektet ”Effektivare granskningsprocess” med målet att standardisera och digitalisera granskningsprocessen av handlingar. Projektet, som finansierades av Svenska Byggbranschens Utvecklingsfond (SBUF), har fått stort genomslag i branschen. Slutrapporten, och dess bilagor, beskriver en granskningsprocess av handlingar i PDF-format med stöd av ett digitalt verktyg. \n \n Genom att höja statusen på objektinformation bör granskningsförfarandet kunna övergå från visuellt granskande till mer regelstyrda kontroller, d.v.s. objektmodeller ska kunna svara på ställda frågor av typen ”Uppfyller väggarna ljudkraven med hänsyn till den verksamhet som planeras pågå i anslutande utrymme?”, eller ”Uppfyller vi kraven på maximalt avstånd till närmaste utrymningsväg etc. \n \n Syftet med denna rapport är att skapa en process för granskning av objektsmodeller som bygger på öppna standarder, och på så sätt kunna vara tillämpbar med de systemstöd som stödjer dessa standarder.",
        type: "Rapport",
        edition: "1",
        language: "Svenska",
        authors: ["Håkan Norberg"],
        publisher: "BIM Alliance",
        published: 2019,
        imageUrl: "https://www.bimalliance.se/library/4885/bim-cmyk.png",
        links: [{
          name: "BIM Alliance",
          uri: "/assets/docs/methods/granskning-av-modellbaserat-underlag-bim-alliance.pdf",
        }],
        tags: [
          {
            short_name: "Fastställd",
            full_name: "Fastställd som en nationell riktlinje.",
            description: "Indikerar att referensen är kvalificerad som en fastställd i förvaltningsstadgarna för nationella riktlinjers referensbibliotek.",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BIM Grunder Sverige",
        description: "Behovet av att utbilda ingenjörer såväl som praktiker med kunskap om och metoder för ett digitaliserat flöde längs byggprocessen blir allt viktigare för att upprätthålla konkurrenskraft globalt och säkerställa kompetenskraft lokalt. Lärosäten, såsom universitet och högskolor i Sverige, utbildar idag civilingenjörer, högskoleingenjörer samt bygg- och förvaltningsexamina på högskolenivå. \n \n Lärosätena har uttalat behovet om att upprätthålla och utveckla utbildningen med en kunskapsbas som bör utgå från gemensamma grunder, begrepp och tillämpningar gällande BIM-relaterade inslag inom utbildningen. \n \n BIM Grunder Sverige är initierat inom BIM Akademin vilket tillhör BIM Alliance och ska fungera som en gemensam bas för kunskapsdelning och utveckling för nationella intressen.",
        type: "Webbplats",
        edition: "1",
        language: "Svenska",
        authors: ["Gustav Jansson"],
        publisher: "BIM Alliance",
        published: 2019,
        imageUrl: "https://www.bimalliance.se/library/4885/bim-cmyk.png",
        links: [{
          name: "BIM Alliance",
          uri: "https://www.bimalliance.se/naetverk-och-moeten/intressentgrupper/bim-akademin/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "Scottish Futures Trust",
        description: "Under arbete.",
        type: "Webbplats",
        edition: "1",
        language: "Engelska",
        authors: ["Scottish Futures Trust"],
        publisher: "Scottish Futures Trust",
        links: [{
          name: "Scottish Futures Trust",
          uri: "https://bimportal.scottishfuturestrust.org.uk/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "CoBIM (Finland)",
        description: "Under arbete.",
        type: "Webbplats",
        edition: "1",
        language: "Engelska",
        authors: ["buildingSMART Finland"],
        publisher: "buildingSMART Finland",
        imageUrl: "https://buildingsmart.fi/wp-content/uploads/2016/09/Logo_finland.jpg",
        links: [{
          name: "CoBIM",
          uri: "https://buildingsmart.fi/en/common-bim-requirements-2012/",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      },
      {
        name: "BIM Loket - Handbok för grundläggande informationsleveranser",
        description: "Nederländska 'BIM-Loket' - En sektorsdriven förening som arbetar mot ökad användning av öppna BIM-standarder, har ihop med buildingSMART Benelux arbetat fram en grundläggande och principiell handbok för grundläggande informationsleverenser. Handboken är ingen ny standard, utan svarar snarare på frågan: 'Hur kan vi dela och utväxla strukturerad och tydlig information under hela byggnadens eller anläggningens livscykel?'",
        type: "Handbok",
        edition: "1",
        language: "Nederländska, Engelska, Svenska",
        authors: ["BIM Loket"],
        publisher: "BIM Loket",
        published: 2019,
        imageUrl: "https://www.bimloket.nl/upload/images/logos/bimloketlogo-175.jpg",
        links: [{
          name: "BIM Loket",
          uri: "https://www.bimloket.nl/upload/documents/downloads/BIMbasisILS/BIM%20basis%20ILS%20SE.pdf",
        }],
        tags: [
          {
            short_name: "För information",
            full_name: "För information",
            description: "Sidoinformation av intresse",
            status: "complete",
            percentDone: 1
          }
        ]
      }
    ]);
  } 
}

export interface NrbReference {
  name: string,
  description: string,
  type: string,
  /**
   * Swe: Utgåva
   */
  edition: string,
  authors: [string],
  publisher?: string,
  published?: number,
  language?: string,
  links?: {
    name: string,
    uri: string
  }[],
  imageUrl?: string
  tags?: {
    short_name: string,
    full_name: string,
    description: string,
    status: string,
    percentDone: number
  }[]
}