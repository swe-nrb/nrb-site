import { Component, OnInit } from '@angular/core';
import { MethodService, NrbMethod } from '../../../../services/method.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-method-show',
  templateUrl: './method-show.component.html',
  styleUrls: ['./method-show.component.css']
})
export class MethodShowComponent implements OnInit {
  noResult: boolean = false;
  method: NrbMethod
  constructor(
    private methodService: MethodService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.methodService
      .getBySlug(this.route.snapshot.params.slug)
      .subscribe((method) => {
        if(!method) {
          this.noResult = true;
        }
        this.method = method;
      }, (err) => {
        console.log(err);
    });
  }

}
