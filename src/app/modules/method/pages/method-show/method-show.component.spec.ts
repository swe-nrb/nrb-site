import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MethodShowComponent } from './method-show.component';

describe('MethodShowComponent', () => {
  let component: MethodShowComponent;
  let fixture: ComponentFixture<MethodShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MethodShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
