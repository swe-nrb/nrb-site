
import {of as observableOf,  Observable ,  merge } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { MethodService } from '../../../../services/method.service';
import { FormControl } from '@angular/forms';
import * as _ from 'lodash';

import { switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-method',
  templateUrl: './method.component.html',
  styleUrls: ['./method.component.css']
})
export class MethodComponent implements OnInit {
  /**
   * Stream of method list
   */
  methods$: Observable<any[]>
  /**
   * Search box
   */
  methodSearchControl: FormControl
  /**
   * Methods count property
   */
  count: Number
  sourceUrl: String
  constructor(
    private methodService: MethodService
  ) {
    this.sourceUrl = 'https://gitlab.com/swe-nrb/sbp-metoder/blob/master/raw_files/methods/';
    this.count = 0;
    this.methodSearchControl = new FormControl();

    this.methods$ = merge(
      observableOf(''),
      this.methodSearchControl.valueChanges
    ).pipe(switchMap(() => {
      // all value changes should trigger list releod
      return this.methodService.list();
    }),
    map(methods => {
      this.count = methods.length;
      var filteredMethods = methods;
      if(this.methodSearchControl.value) {
        filteredMethods = _.filter(methods, (method) => {
          return _.get(method, 'name.sv.name', '').toLowerCase().indexOf(this.methodSearchControl.value.toLowerCase()) >= 0;
        });
      }
      return filteredMethods;
    }),
    map((filteredMethods) => {
      var sorted =  _.sortBy(filteredMethods, (o) => {
        return _.get(o, 'name.sv.name', '');
      });
      return sorted.splice(0,20);
    }),);

  }

  ngOnInit() { }

  goToSource(filename) {
    window.open(this.sourceUrl + filename);
  }

}
