import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MethodComponent } from './method.component';
import { MaterialModule } from '../../../../material/material.module';
import { PartialsModule } from '../../../../partials/partials.module';
import { MethodService } from '../../../../services/method.service';
import { HttpClientModule } from '@angular/common/http';

describe('MethodComponent', () => {
  let component: MethodComponent;
  let fixture: ComponentFixture<MethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        PartialsModule,
        HttpClientModule
      ],
      declarations: [ MethodComponent ],
      providers: [MethodService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
