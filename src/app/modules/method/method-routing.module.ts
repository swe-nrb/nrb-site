import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MethodComponent } from './pages/method-list/method.component';
import { MethodShowComponent } from './pages/method-show/method-show.component';

const routes: Routes = [{
  path: 'methods',
  component: MethodComponent
}, {
  path: 'methods/:slug',
  component: MethodShowComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MethodRoutingModule { }
