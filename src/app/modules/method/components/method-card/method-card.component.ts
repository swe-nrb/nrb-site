import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ClipboardService } from 'ngx-clipboard';

@Component({
  selector: 'nrb-method-card',
  templateUrl: './method-card.component.html',
  styleUrls: ['./method-card.component.css']
})
export class MethodCardComponent implements OnInit {
  @Input() method
  constructor(
    private snackbar: MatSnackBar,
    private clip: ClipboardService
  ) { }

  ngOnInit() {
  }

  copyToClip(slug): void {
    this.clip.copyFromContent(`http://www.nationella-riktlinjer.se/methods/${slug}`);
    this.snackbar.open('Din länk är färdig att klistra in.', '', { duration: 2000 });
  }

}
