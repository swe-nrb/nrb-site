import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MethodRoutingModule } from './method-routing.module';
import { MethodComponent } from './pages/method-list/method.component';
import { MaterialModule } from '../../material/material.module';
import { PartialsModule } from '../../partials/partials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { SharedModule } from '../shared/shared.module';
import { MethodCardComponent } from './components/method-card/method-card.component';
import { MethodShowComponent } from './pages/method-show/method-show.component';

@NgModule({
  imports: [
    CommonModule,
    MarkdownModule.forChild(),
    MethodRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PartialsModule,
    SharedModule
  ],
  declarations: [MethodComponent, MethodCardComponent, MethodShowComponent]
})
export class MethodModule { }
