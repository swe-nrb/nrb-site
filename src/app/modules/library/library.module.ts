import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryRoutingModule } from './library-routing.module';
import { LibraryListComponent } from './library-list/library-list.component';
import { LibraryShowComponent } from './library-show/library-show.component';
import { SharedModule } from '../shared/shared.module';
import { ReferenceCardComponent } from './components/reference-card/reference-card.component';
import { MarkdownModule } from 'ngx-markdown';

@NgModule({
  imports: [
    CommonModule,
    LibraryRoutingModule,
    SharedModule,
    MarkdownModule
  ],
  declarations: [
    LibraryListComponent,
    LibraryShowComponent,
    ReferenceCardComponent
  ]
})
export class LibraryModule { }
