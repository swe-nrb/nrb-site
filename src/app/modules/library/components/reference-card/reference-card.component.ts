import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nrb-reference-card',
  templateUrl: './reference-card.component.html',
  styleUrls: ['./reference-card.component.css']
})
export class ReferenceCardComponent implements OnInit {
  @Input() reference: any
  sourceUrl: string
  constructor() { }

  ngOnInit() {
  }

  goToSource(concept) {
    window.open(this.sourceUrl + concept.slug + '.json');
  }

  ngOnDestroy() {}
}
