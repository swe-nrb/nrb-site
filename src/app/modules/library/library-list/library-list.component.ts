import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ReferenceService, NrbReference } from '../../../services/reference.service';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
  selector: 'app-library-list',
  templateUrl: './library-list.component.html',
  styleUrls: ['./library-list.component.css']
})
export class LibraryListComponent implements OnInit {
  references$: Observable<NrbReference[]>
  constructor(
    public referenceService: ReferenceService
  ) { }

  ngOnInit() {
    this.references$ = this.referenceService
      .list()
      .pipe(map(list => _.sortBy(list, 'name')))
  }

}
