import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryListComponent } from './library-list/library-list.component';
import { LibraryShowComponent } from './library-show/library-show.component';

const routes: Routes = [{
  path: 'library/:slug',
  component: LibraryShowComponent
}, {
  path: 'library',
  component: LibraryListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibraryRoutingModule { }
