import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbColumnsComponent } from './nrb-columns.component';

describe('NrbColumnsComponent', () => {
  let component: NrbColumnsComponent;
  let fixture: ComponentFixture<NrbColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
