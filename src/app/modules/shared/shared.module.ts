import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NrbCardComponent } from './nrb-card/nrb-card.component';
import { MaterialModule } from '../../material/material.module';
import { RouterModule } from '@angular/router';
import { UnderConstructionComponent } from './under-construction/under-construction.component';
import { NrbPageContainerComponent } from './nrb-page-container/nrb-page-container.component';
import { NrbArticleComponent } from './nrb-article/nrb-article.component';
import { NrbArticleTextComponent } from './nrb-article-text/nrb-article-text.component';
import { NrbArticleImageComponent } from './nrb-article-image/nrb-article-image.component';
import { MarkdownModule } from 'ngx-markdown';
import { NrbPageNavigationComponent } from './nrb-page-navigation/nrb-page-navigation.component';
import { NrbVersionComponent } from './nrb-version/nrb-version.component';
import { NrbColumnsComponent } from './nrb-columns/nrb-columns.component';
import { NrbTagDialogComponent } from './nrb-tag-dialog/nrb-tag-dialog.component';
import { NrbTagComponent } from './nrb-tag/nrb-tag.component';
import { NrbTagsComponent } from './nrb-tags/nrb-tags.component';
import { NrbResourceCardComponent } from './nrb-resource-card/nrb-resource-card.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    MarkdownModule
  ],
  declarations: [
    NrbCardComponent,
    UnderConstructionComponent,
    NrbPageContainerComponent,
    NrbArticleComponent,
    NrbArticleTextComponent,
    NrbArticleImageComponent,
    NrbPageNavigationComponent,
    NrbVersionComponent,
    NrbColumnsComponent,
    NrbTagDialogComponent,
    NrbTagComponent,
    NrbTagsComponent,
    NrbResourceCardComponent
  ],
  exports: [
    NrbCardComponent,
    UnderConstructionComponent,
    NrbPageContainerComponent,
    NrbArticleComponent,
    NrbArticleTextComponent,
    NrbArticleImageComponent,
    NrbPageNavigationComponent,
    NrbVersionComponent,
    NrbColumnsComponent,
    NrbTagDialogComponent,
    NrbTagComponent,
    NrbTagsComponent,
    NrbResourceCardComponent
  ],
  entryComponents: [
    NrbTagDialogComponent
  ]
})
export class SharedModule { }
