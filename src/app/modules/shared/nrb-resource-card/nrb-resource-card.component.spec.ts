import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbResourceCardComponent } from './nrb-resource-card.component';

describe('NrbResourceCardComponent', () => {
  let component: NrbResourceCardComponent;
  let fixture: ComponentFixture<NrbResourceCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbResourceCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbResourceCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
