import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'nrb-resource-card',
  templateUrl: './nrb-resource-card.component.html',
  styleUrls: ['./nrb-resource-card.component.css']
})
export class NrbResourceCardComponent implements OnInit, OnDestroy {
  @Input() resource: any
  @Input() url: string
  /**
   * which key will be used to display main title on card
   */
  @Input() title: string = 'term'
  @Input() sourceUrl: string
  constructor(
    private snackbar: MatSnackBar,
    private clip: ClipboardService
  ) { }

  ngOnInit() { }

  goToSource(resource) {
    window.open(this.sourceUrl + resource.slug + '.json');
  }

  ngOnDestroy() {}

  copyToClip(resourceSlug): void {
    this.clip.copyFromContent(`http://www.nationella-riktlinjer.se/${this.url}${resourceSlug}`);
    this.snackbar.open('Din länk är färdig att klistra in.', '', { duration: 2000 });
  }

}
