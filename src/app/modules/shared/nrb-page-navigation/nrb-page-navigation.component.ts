import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nrb-page-navigation',
  templateUrl: './nrb-page-navigation.component.html',
  styleUrls: ['./nrb-page-navigation.component.css']
})
export class NrbPageNavigationComponent implements OnInit {
  @Input() links: NrbPageNavigationLink[]
  constructor() { }

  ngOnInit() {
    
  }
}

export interface NrbPageNavigationLink {
  url: string,
  label: string
}