import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbPageNavigationComponent } from './nrb-page-navigation.component';

describe('NrbPageNavigationComponent', () => {
  let component: NrbPageNavigationComponent;
  let fixture: ComponentFixture<NrbPageNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbPageNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbPageNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
