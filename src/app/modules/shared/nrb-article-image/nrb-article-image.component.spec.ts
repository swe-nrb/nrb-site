import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbArticleImageComponent } from './nrb-article-image.component';

describe('NrbArticleImageComponent', () => {
  let component: NrbArticleImageComponent;
  let fixture: ComponentFixture<NrbArticleImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbArticleImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbArticleImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
