import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'nrb-article-image',
  templateUrl: './nrb-article-image.component.html',
  styleUrls: ['./nrb-article-image.component.css']
})
export class NrbArticleImageComponent implements AfterViewInit {
  @Input() url: string
  @Input() padding: string = "0px";
  @Input() height: string = "300px";
  @Input() backgroundSize: string = 'cover';
  @ViewChild('img', { static: true }) el: ElementRef
  constructor(
    private elRef: ElementRef
  ) { }

  ngAfterViewInit() {
    this.elRef.nativeElement.style['padding'] = this.padding
    this.el.nativeElement.style['height'] = this.height;
    this.el.nativeElement.style['background-image'] =  "url('" + this.url + "')";
    this.el.nativeElement.style['background-size'] =  this.backgroundSize;
  }

}
