import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbArticleComponent } from './nrb-article.component';

describe('NrbArticleComponent', () => {
  let component: NrbArticleComponent;
  let fixture: ComponentFixture<NrbArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
