import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nrb-card',
  templateUrl: './nrb-card.component.html',
  styleUrls: ['./nrb-card.component.css']
})
export class NrbCardComponent implements OnInit {
  @Input() footer: any
  @Input() headerImage: string
  @Input() headerHeight: string
  headerStyle: any
  constructor() {
    this.headerStyle = {
      height: '210px'
    };
  }

  ngOnInit() {
    if(this.headerImage) this.headerStyle["background-image"] = `url(${this.headerImage})`;
    if(this.headerHeight) this.headerStyle["height"] = this.headerHeight;
  }
}
