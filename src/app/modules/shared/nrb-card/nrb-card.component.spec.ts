import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbCardComponent } from './nrb-card.component';

describe('NrbCardComponent', () => {
  let component: NrbCardComponent;
  let fixture: ComponentFixture<NrbCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
