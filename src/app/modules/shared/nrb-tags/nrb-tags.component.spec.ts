import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentTagsComponent } from './component-tags.component';

describe('ComponentTagsComponent', () => {
  let component: ComponentTagsComponent;
  let fixture: ComponentFixture<ComponentTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
