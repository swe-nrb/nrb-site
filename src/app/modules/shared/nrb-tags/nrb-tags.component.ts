import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nrb-tags',
  templateUrl: './nrb-tags.component.html',
  styleUrls: ['./nrb-tags.component.css']
})
export class NrbTagsComponent implements OnInit {
  @Input() tags: any[]
  constructor() {
  }

  ngOnInit() {
    if(this.tags.length < 1) {
      // set a default tag
      this.tags = []
    }
  }
}
