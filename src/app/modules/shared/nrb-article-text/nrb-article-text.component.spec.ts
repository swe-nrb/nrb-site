import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbArticleTextComponent } from './nrb-article-text.component';

describe('NrbArticleTextComponent', () => {
  let component: NrbArticleTextComponent;
  let fixture: ComponentFixture<NrbArticleTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbArticleTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbArticleTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
