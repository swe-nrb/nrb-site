import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentTagComponent } from './nrb-tag.component';

describe('ComponentTagsComponent', () => {
  let component: ComponentTagComponent;
  let fixture: ComponentFixture<ComponentTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
