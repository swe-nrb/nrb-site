import { Component, OnInit, HostListener, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NrbTagDialogComponent } from '../nrb-tag-dialog/nrb-tag-dialog.component';

@Component({
  selector: 'nrb-tag',
  templateUrl: './nrb-tag.component.html',
  styleUrls: ['./nrb-tag.component.css']
})
export class NrbTagComponent implements OnInit {
  @Input() tag: any
  svgIcon: String
  constructor(
    public dialog: MatDialog
  ) {
    this.svgIcon = 'nrb:tag-information';
  }

  ngOnInit() { }

  @HostListener('click', ['$event'])
  openDialog(): void {
    const dialogRef = this.dialog.open(NrbTagDialogComponent, {
      width: '450px',
      data: this.tag
    });

    dialogRef
      .afterClosed()
      .subscribe(result => { });
  }

}
