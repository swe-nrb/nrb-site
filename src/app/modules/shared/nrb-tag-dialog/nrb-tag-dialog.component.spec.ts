import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbTagDialogComponent } from './nrb-tag-dialog.component';

describe('NrbTagDialogComponent', () => {
  let component: NrbTagDialogComponent;
  let fixture: ComponentFixture<NrbTagDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbTagDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbTagDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
