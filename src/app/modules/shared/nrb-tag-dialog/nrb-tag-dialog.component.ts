import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'nrb-tag-dialog',
  templateUrl: './nrb-tag-dialog.component.html',
  styleUrls: ['./nrb-tag-dialog.component.css']
})
export class NrbTagDialogComponent implements OnInit {
  statusClass: any
  constructor(
    @Inject(MAT_DIALOG_DATA) public tag: any
  ) { }

  ngOnInit() {
    this.statusClass = {
      "status-idle": (this.tag.status == 'idle'),
      "status-pending": (this.tag.status == 'pending'),
      "status-complete": (this.tag.status == 'complete')
    };
  }

}
