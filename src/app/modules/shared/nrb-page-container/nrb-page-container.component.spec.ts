import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbPageContainerComponent } from './nrb-page-container.component';

describe('NrbPageContainerComponent', () => {
  let component: NrbPageContainerComponent;
  let fixture: ComponentFixture<NrbPageContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbPageContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
