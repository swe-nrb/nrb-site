import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nrb-page-container',
  templateUrl: './nrb-page-container.component.html',
  styleUrls: ['./nrb-page-container.component.css']
})
export class NrbPageContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
