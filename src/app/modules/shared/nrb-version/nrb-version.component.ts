import { Component, OnInit } from '@angular/core';
import { version as packageVersion } from '../../../../../package.json';

@Component({
  selector: 'nrb-version',
  templateUrl: './nrb-version.component.html',
  styleUrls: ['./nrb-version.component.css']
})
export class NrbVersionComponent implements OnInit {
  version: string
  constructor() { }

  ngOnInit() {
    this.version = packageVersion;
  }

}
