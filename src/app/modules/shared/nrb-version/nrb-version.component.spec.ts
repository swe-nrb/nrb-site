import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NrbVersionComponent } from './nrb-version.component';

describe('NrbVersionComponent', () => {
  let component: NrbVersionComponent;
  let fixture: ComponentFixture<NrbVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NrbVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NrbVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
