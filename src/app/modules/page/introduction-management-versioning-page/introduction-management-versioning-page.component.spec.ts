import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionManagementVersioningPageComponent } from './introduction-management-versioning-page.component';

describe('IntroductionManagementVersioningPageComponent', () => {
  let component: IntroductionManagementVersioningPageComponent;
  let fixture: ComponentFixture<IntroductionManagementVersioningPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionManagementVersioningPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionManagementVersioningPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
