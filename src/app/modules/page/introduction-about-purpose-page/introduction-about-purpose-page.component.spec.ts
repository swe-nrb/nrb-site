import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutPurposePageComponent } from './introduction-about-purpose-page.component';

describe('IntroductionAboutPurposePageComponent', () => {
  let component: IntroductionAboutPurposePageComponent;
  let fixture: ComponentFixture<IntroductionAboutPurposePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutPurposePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutPurposePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
