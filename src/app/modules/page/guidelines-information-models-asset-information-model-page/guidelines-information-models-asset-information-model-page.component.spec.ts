import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidelinesInformationModelsAssetInformationModelPageComponent } from './guidelines-information-models-asset-information-model-page.component';

describe('GuidelinesInformationModelsAssetInformationModelPageComponent', () => {
  let component: GuidelinesInformationModelsAssetInformationModelPageComponent;
  let fixture: ComponentFixture<GuidelinesInformationModelsAssetInformationModelPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidelinesInformationModelsAssetInformationModelPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidelinesInformationModelsAssetInformationModelPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
