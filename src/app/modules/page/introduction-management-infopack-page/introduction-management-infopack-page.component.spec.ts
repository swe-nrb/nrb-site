import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionManagementInfopackPageComponent } from './introduction-management-infopack-page.component';

describe('IntroductionManagementInfopackPageComponent', () => {
  let component: IntroductionManagementInfopackPageComponent;
  let fixture: ComponentFixture<IntroductionManagementInfopackPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionManagementInfopackPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionManagementInfopackPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
