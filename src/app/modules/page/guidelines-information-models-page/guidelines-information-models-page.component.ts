import { Component, OnInit } from '@angular/core';
import { NrbPageNavigationLink } from '../../shared/nrb-page-navigation/nrb-page-navigation.component';

@Component({
  selector: 'app-guidelines-information-models-page',
  templateUrl: './guidelines-information-models-page.component.html',
  styleUrls: ['./guidelines-information-models-page.component.css']
})
export class GuidelinesInformationModelsPageComponent implements OnInit {
  links: NrbPageNavigationLink[]
  constructor() {
    this.links = [{
      url: 'asset-information-model',
      label: 'Tillgångsinformationsmodell'
    }, {
      url: 'project-information-model',
      label: 'Projektinformationsmodell'
    }];
  }
  ngOnInit() { }
}
