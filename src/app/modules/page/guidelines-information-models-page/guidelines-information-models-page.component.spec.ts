import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidelinesInformationModelsPageComponent } from './guidelines-information-models-page.component';

describe('GuidelinesInformationModelsPageComponent', () => {
  let component: GuidelinesInformationModelsPageComponent;
  let fixture: ComponentFixture<GuidelinesInformationModelsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidelinesInformationModelsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidelinesInformationModelsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
