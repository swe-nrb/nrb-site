import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementIntroPageComponent } from './requirement-intro-page.component';

describe('RequirementIntroPageComponent', () => {
  let component: RequirementIntroPageComponent;
  let fixture: ComponentFixture<RequirementIntroPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementIntroPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementIntroPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
