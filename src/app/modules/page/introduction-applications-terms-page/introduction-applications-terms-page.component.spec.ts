import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionApplicationsTermsPageComponent } from './introduction-applications-terms-page.component';

describe('IntroductionApplicationsTermsPageComponent', () => {
  let component: IntroductionApplicationsTermsPageComponent;
  let fixture: ComponentFixture<IntroductionApplicationsTermsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionApplicationsTermsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionApplicationsTermsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
