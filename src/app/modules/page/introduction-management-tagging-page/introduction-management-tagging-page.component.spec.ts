import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionManagementTaggingPageComponent } from './introduction-management-tagging-page.component';

describe('IntroductionManagementTaggingPageComponent', () => {
  let component: IntroductionManagementTaggingPageComponent;
  let fixture: ComponentFixture<IntroductionManagementTaggingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionManagementTaggingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionManagementTaggingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
