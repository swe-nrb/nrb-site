import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutIntroductionPageComponent } from './introduction-about-introduction-page.component';

describe('IntroductionAboutIntroductionPageComponent', () => {
  let component: IntroductionAboutIntroductionPageComponent;
  let fixture: ComponentFixture<IntroductionAboutIntroductionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutIntroductionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutIntroductionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
