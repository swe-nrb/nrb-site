import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidelinesInformationModelsProjectInformationModelPageComponent } from './guidelines-information-models-project-information-model-page.component';

describe('GuidelinesInformationModelsProjectInformationModelPageComponent', () => {
  let component: GuidelinesInformationModelsProjectInformationModelPageComponent;
  let fixture: ComponentFixture<GuidelinesInformationModelsProjectInformationModelPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidelinesInformationModelsProjectInformationModelPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidelinesInformationModelsProjectInformationModelPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
