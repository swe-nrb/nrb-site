import { Component, OnInit } from '@angular/core';
import { NrbPageNavigationLink } from '../../shared/nrb-page-navigation/nrb-page-navigation.component';

@Component({
  selector: 'app-requirements-page',
  templateUrl: './requirements-page.component.html',
  styleUrls: ['./requirements-page.component.css']
})
export class RequirementsPageComponent implements OnInit {
  links: NrbPageNavigationLink[]
  constructor() {
    this.links = [{
      url: 'intro',
      label: 'Informationskrav'
    }, {
      url: 'organization',
      label: 'Organisationens informationskrav'
    }, {
      url: 'asset',
      label: 'Tillgångsinformationskrav'
    }, {
      url: 'project',
      label: 'Projektinformationskrav'
    }, {
      url: 'exchange',
      label: 'Uppdragets informationskrav'
    }];
  }

  ngOnInit() { }
}
