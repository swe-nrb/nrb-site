import { Component, OnInit } from '@angular/core';
import { NrbPageNavigationLink } from '../../shared/nrb-page-navigation/nrb-page-navigation.component';

@Component({
  selector: 'app-introduction-management-page',
  templateUrl: './introduction-management-page.component.html',
  styleUrls: ['./introduction-management-page.component.css']
})
export class IntroductionManagementPageComponent implements OnInit {
  links: NrbPageNavigationLink[]
  constructor() {
    this.links = [{
      url: 'management',
      label: 'Förvaltning'
    }, {
      url: 'infopack',
      label: 'Infopack'
    }, {
      url: 'publication-process',
      label: 'Publikationsprocessen'
    }, {
      url: 'tagging',
      label: 'Taggning'
    }, {
      url: 'versioning',
      label: 'Versionshantering'
    }];
  }
  ngOnInit() { }
}
