import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionManagementPageComponent } from './introduction-management-page.component';

describe('IntroductionManagementPageComponent', () => {
  let component: IntroductionManagementPageComponent;
  let fixture: ComponentFixture<IntroductionManagementPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionManagementPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionManagementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
