import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-introduction-page',
  template: '<router-outlet></router-outlet>'
})
export class IntroductionPageComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

}
