import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionManagementManagementPageComponent } from './introduction-management-management-page.component';

describe('IntroductionManagementManagementPageComponent', () => {
  let component: IntroductionManagementManagementPageComponent;
  let fixture: ComponentFixture<IntroductionManagementManagementPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionManagementManagementPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionManagementManagementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
