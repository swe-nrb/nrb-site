import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionApplicationsGitlabPageComponent } from './introduction-applications-gitlab-page.component';

describe('IntroductionApplicationsGitlabPageComponent', () => {
  let component: IntroductionApplicationsGitlabPageComponent;
  let fixture: ComponentFixture<IntroductionApplicationsGitlabPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionApplicationsGitlabPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionApplicationsGitlabPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
