import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutGuidelinesPageComponent } from './introduction-about-guidelines-page.component';

describe('IntroductionAboutGuidelinesPageComponent', () => {
  let component: IntroductionAboutGuidelinesPageComponent;
  let fixture: ComponentFixture<IntroductionAboutGuidelinesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutGuidelinesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutGuidelinesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
