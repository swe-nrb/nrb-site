import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidelinesOrganizationPageComponent } from './guidelines-organization-page.component';

describe('GuidelinesOrganizationPageComponent', () => {
  let component: GuidelinesOrganizationPageComponent;
  let fixture: ComponentFixture<GuidelinesOrganizationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidelinesOrganizationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidelinesOrganizationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
