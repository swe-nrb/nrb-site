import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutScopePageComponent } from './introduction-about-scope-page.component';

describe('IntroductionAboutScopePageComponent', () => {
  let component: IntroductionAboutScopePageComponent;
  let fixture: ComponentFixture<IntroductionAboutScopePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutScopePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutScopePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
