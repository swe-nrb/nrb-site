import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StandardsComponent } from './standards/standards.component';
import { ContactComponent } from './contact/contact.component';
import { IntroductionPageComponent } from './introduction-page/introduction-page.component';
import { GuidelinesPageComponent } from './guidelines-page/guidelines-page.component';
import { IntroductionAboutPageComponent } from './introduction-about-page/about-page.component';
import { IntroductionAboutVisionPageComponent } from './introduction-about-vision-page/vision-page.component';
import { RequirementsPageComponent } from './guidelines-requirements-page/requirements-page.component';
import { OrganizationPageComponent } from './guidelines-requirements-organization-page/organization-page.component';
import { AssetPageComponent } from './guidelines-requirements-asset-page/asset-page.component';
import { ProjectPageComponent } from './guidelines-requirements-project-page/project-page.component';
import { ExchangePageComponent } from './guidelines-requirements-exchange-page/exchange-page.component';
import { RequirementIntroPageComponent } from './guidelines-requirements-intro-page/requirement-intro-page.component';
import { IntroductionAboutPurposePageComponent } from './introduction-about-purpose-page/introduction-about-purpose-page.component';
import { IntroductionManagementPageComponent } from './introduction-management-page/introduction-management-page.component';
import { IntroductionManagementManagementPageComponent } from './introduction-management-management-page/introduction-management-management-page.component';
import { IntroductionApplicationsPageComponent } from './introduction-applications-page/introduction-applications-page.component';
import { IntroductionApplicationsTermsPageComponent } from './introduction-applications-terms-page/introduction-applications-terms-page.component';
import { IntroductionApplicationsGitlabPageComponent } from './introduction-applications-gitlab-page/introduction-applications-gitlab-page.component';
import { IntroductionManagementPublicationProcessPageComponent } from './introduction-management-publication-process-page/introduction-management-publication-process-page.component';
import { IntroductionManagementTaggingPageComponent } from './introduction-management-tagging-page/introduction-management-tagging-page.component';
import { IntroductionManagementInfopackPageComponent } from './introduction-management-infopack-page/introduction-management-infopack-page.component';
import { GuidelinesInformationModelsPageComponent } from './guidelines-information-models-page/guidelines-information-models-page.component';
import { GuidelinesInformationModelsAssetInformationModelPageComponent } from './guidelines-information-models-asset-information-model-page/guidelines-information-models-asset-information-model-page.component';
import { GuidelinesInformationModelsProjectInformationModelPageComponent } from './guidelines-information-models-project-information-model-page/guidelines-information-models-project-information-model-page.component';
import { GuidelinesProcessesPageComponent } from './guidelines-processes-page/guidelines-processes-page.component';
import { GuidelinesOrganizationPageComponent } from './guidelines-organization-page/guidelines-organization-page.component';
import { IntroductionAboutScopePageComponent } from './introduction-about-scope-page/introduction-about-scope-page.component';
import { IntroductionAboutAudiencePageComponent } from './introduction-about-audience-page/introduction-about-audience-page.component';
import { IntroductionAboutGuidelinesPageComponent } from './introduction-about-guidelines-page/introduction-about-guidelines-page.component';
import { IntroductionAboutIntroductionPageComponent } from './introduction-about-introduction-page/introduction-about-introduction-page.component';
import { IntroductionApplicationsApplicationsPageComponent } from './introduction-applications-applications-page/introduction-applications-applications-page.component';
import { IntroductionManagementVersioningPageComponent } from './introduction-management-versioning-page/introduction-management-versioning-page.component';

const routes: Routes = [
  // introduction
  {
    path: 'introduction',
    component: IntroductionPageComponent,
    children: [{
      path: 'about',
      component: IntroductionAboutPageComponent,
      children: [{
        path: 'introduction',
        component: IntroductionAboutIntroductionPageComponent
      }, {
        path: 'vision',
        component: IntroductionAboutVisionPageComponent
      }, {
        path: 'purpose',
        component: IntroductionAboutPurposePageComponent
      }, {
        path: 'scope',
        component: IntroductionAboutScopePageComponent
      }, {
        path: 'audience',
        component: IntroductionAboutAudiencePageComponent
      }, {
        path: 'guidelines',
        component: IntroductionAboutGuidelinesPageComponent
      }, {
        path: '',
        redirectTo: 'introduction',
        pathMatch: 'full'
      }]
    }, {
      path: 'management',
      component: IntroductionManagementPageComponent,
      children: [{
        path: 'management',
        component: IntroductionManagementManagementPageComponent
      }, {
        path: 'infopack',
        component: IntroductionManagementInfopackPageComponent
      }, {
        path: 'publication-process',
        component: IntroductionManagementPublicationProcessPageComponent
      }, {
        path: 'tagging',
        component: IntroductionManagementTaggingPageComponent
      }, {
        path: 'versioning',
        component: IntroductionManagementVersioningPageComponent
      }, {
        path: '',
        redirectTo: 'management',
        pathMatch: 'full'
      }]
    }, {
      path: 'applications',
      component: IntroductionApplicationsPageComponent,
      children: [{
        path: 'terms',
        component: IntroductionApplicationsTermsPageComponent
      }, {
        path: 'gitlab',
        component: IntroductionApplicationsGitlabPageComponent
      }, {
        path: 'applications',
        component: IntroductionApplicationsApplicationsPageComponent
      }, {
        path: '',
        redirectTo: 'terms',
        pathMatch: 'full'
      }]
    }, {
      path: '',
      redirectTo: 'about',
      pathMatch: 'full'
    }]
  },
  // guidelines
  {
    path: 'guidelines',
    component: GuidelinesPageComponent,
    children: [
      {
        path: 'requirements',
        component: RequirementsPageComponent,
        children: [{
          path: 'intro',
          component: RequirementIntroPageComponent
        }, {
          path: 'organization',
          component: OrganizationPageComponent
        }, {
          path: 'asset',
          component: AssetPageComponent
        }, {
          path: 'project',
          component: ProjectPageComponent
        }, {
          path: 'exchange',
          component: ExchangePageComponent
        }, {
          path: '',
          redirectTo: 'intro',
          pathMatch: 'full'
        }]
      },
      {
        path: 'information-models',
        component: GuidelinesInformationModelsPageComponent,
        children: [{
          path: 'asset-information-model',
          component: GuidelinesInformationModelsAssetInformationModelPageComponent
        }, {
          path: 'project-information-model',
          component: GuidelinesInformationModelsProjectInformationModelPageComponent
        }, {
          path: '',
          redirectTo: 'asset-information-model',
          pathMatch: 'full'
        }]
      },
      {
        path: 'processes',
        component: GuidelinesProcessesPageComponent
      },
      {
        path: 'organization',
        component: GuidelinesOrganizationPageComponent
      },
      {
        path: '',
        redirectTo: 'requirements',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'standards',
    component: StandardsComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
