import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutVisionPageComponent } from './vision-page.component';

describe('VisionPageComponent', () => {
  let component: IntroductionAboutVisionPageComponent;
  let fixture: ComponentFixture<IntroductionAboutVisionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutVisionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutVisionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
