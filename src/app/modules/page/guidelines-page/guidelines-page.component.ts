import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guidelines-page',
  template: '<router-outlet></router-outlet>'
})
export class GuidelinesPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
