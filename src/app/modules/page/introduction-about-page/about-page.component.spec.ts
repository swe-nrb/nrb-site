import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutPageComponent } from './about-page.component';

describe('AboutPageComponent', () => {
  let component: IntroductionAboutPageComponent;
  let fixture: ComponentFixture<IntroductionAboutPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
