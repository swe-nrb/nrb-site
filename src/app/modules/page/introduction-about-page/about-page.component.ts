import { Component, OnInit, Input } from '@angular/core';
import { NrbPageNavigationLink } from '../../shared/nrb-page-navigation/nrb-page-navigation.component';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})
export class IntroductionAboutPageComponent implements OnInit {
  @Input() links: NrbPageNavigationLink[]
  constructor() {
    this.links = [{
      url: 'introduction',
      label: 'Introduktion'
    }, {
      url: 'vision',
      label: 'Vision'
    }, {
      url: 'purpose',
      label: 'Syfte'
    }, {
      url: 'scope',
      label: 'Omfattning'
    }, {
      url: 'audience',
      label: 'Målgrupp'
    }, {
      url: 'guidelines',
      label: 'Riktlinjer?'
    }];
  }

  ngOnInit() {
  }

}
