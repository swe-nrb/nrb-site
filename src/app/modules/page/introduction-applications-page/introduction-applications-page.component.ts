import { Component, OnInit } from '@angular/core';
import { NrbPageNavigationLink } from '../../shared/nrb-page-navigation/nrb-page-navigation.component';

@Component({
  selector: 'app-introduction-applications-page',
  templateUrl: './introduction-applications-page.component.html',
  styleUrls: ['./introduction-applications-page.component.css']
})
export class IntroductionApplicationsPageComponent implements OnInit {
  links: NrbPageNavigationLink[]
  constructor() {
    this.links = [{
      url: 'terms',
      label: 'Nyttjanderätt, ägande och ansvar'
    }, {
      url: 'gitlab',
      label: 'Plattformen på gitlab'
    }, {
      url: 'applications',
      label: 'Tillämpning av Riktlinjerna'
    }];
  }

  ngOnInit() { }
}
