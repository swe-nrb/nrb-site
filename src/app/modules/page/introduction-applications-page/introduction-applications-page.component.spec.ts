import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionApplicationsPageComponent } from './introduction-applications-page.component';

describe('IntroductionApplicationsPageComponent', () => {
  let component: IntroductionApplicationsPageComponent;
  let fixture: ComponentFixture<IntroductionApplicationsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionApplicationsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionApplicationsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
