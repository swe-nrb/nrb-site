import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionApplicationsApplicationsPageComponent } from './introduction-applications-applications-page.component';

describe('IntroductionApplicationsApplicationsPageComponent', () => {
  let component: IntroductionApplicationsApplicationsPageComponent;
  let fixture: ComponentFixture<IntroductionApplicationsApplicationsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionApplicationsApplicationsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionApplicationsApplicationsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
