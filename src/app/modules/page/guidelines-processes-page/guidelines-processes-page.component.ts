import { Component, OnInit } from '@angular/core';
import { NrbPageNavigationLink } from '../../shared/nrb-page-navigation/nrb-page-navigation.component';

@Component({
  selector: 'app-guidelines-processes-page',
  templateUrl: './guidelines-processes-page.component.html',
  styleUrls: ['./guidelines-processes-page.component.css']
})
export class GuidelinesProcessesPageComponent implements OnInit {
  links: NrbPageNavigationLink[]
  constructor() {
    this.links = [{
      url: 'intro',
      label: 'Informationskrav'
    }, {
      url: 'organization',
      label: 'Organisationens informationskrav'
    }, {
      url: 'asset',
      label: 'Tillgångsinformationskrav'
    }, {
      url: 'project',
      label: 'Projektinformationskrav'
    }, {
      url: 'exchange',
      label: 'Uppdragets informationskrav'
    }];
  }

  ngOnInit() { }
}
