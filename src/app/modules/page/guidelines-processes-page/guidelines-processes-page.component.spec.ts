import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidelinesProcessesPageComponent } from './guidelines-processes-page.component';

describe('GuidelinesProcessesPageComponent', () => {
  let component: GuidelinesProcessesPageComponent;
  let fixture: ComponentFixture<GuidelinesProcessesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidelinesProcessesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidelinesProcessesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
