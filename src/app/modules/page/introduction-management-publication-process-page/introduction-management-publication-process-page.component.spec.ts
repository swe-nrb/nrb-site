import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionManagementPublicationProcessPageComponent } from './introduction-management-publication-process-page.component';

describe('IntroductionManagementPublicationProcessPageComponent', () => {
  let component: IntroductionManagementPublicationProcessPageComponent;
  let fixture: ComponentFixture<IntroductionManagementPublicationProcessPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionManagementPublicationProcessPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionManagementPublicationProcessPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
