import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionAboutAudiencePageComponent } from './introduction-about-audience-page.component';

describe('IntroductionAboutAudiencePageComponent', () => {
  let component: IntroductionAboutAudiencePageComponent;
  let fixture: ComponentFixture<IntroductionAboutAudiencePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionAboutAudiencePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionAboutAudiencePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
