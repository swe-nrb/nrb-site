import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-delivery-specification',
  templateUrl: './delivery-specification.component.html',
  styleUrls: ['./delivery-specification.component.css']
})
export class DeliverySpecificationComponent implements OnInit {
  fileList: Observable<any[]>
  constructor() { }

  ngOnInit() {
  }
  goToDocument(doc) {
    window.open('/assets/docs/' + doc, '_blank');
  }
  goToUrl(url) {
    window.open(url, '_blank');
  }
}
