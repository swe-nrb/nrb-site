import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverySpecificationComponent } from './delivery-specification.component';
import { MaterialModule } from '../../material/material.module';
import { PartialsModule } from '../../partials/partials.module';

describe('DeliverySpecificationComponent', () => {
  let component: DeliverySpecificationComponent;
  let fixture: ComponentFixture<DeliverySpecificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        PartialsModule
      ],
      declarations: [ DeliverySpecificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverySpecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
