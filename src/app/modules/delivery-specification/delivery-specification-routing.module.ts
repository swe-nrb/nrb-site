import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliverySpecificationComponent } from './delivery-specification.component';

const routes: Routes = [{
  path: 'delivery-specifications',
  component: DeliverySpecificationComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliverySpecificationRoutingModule { }
