import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliverySpecificationRoutingModule } from './delivery-specification-routing.module';
import { DeliverySpecificationComponent } from './delivery-specification.component';
import { MaterialModule } from '../../material/material.module';
import { PartialsModule } from '../../partials/partials.module';

@NgModule({
  imports: [
    CommonModule,
    DeliverySpecificationRoutingModule,
    MaterialModule,
    PartialsModule
  ],
  declarations: [DeliverySpecificationComponent]
})
export class DeliverySpecificationModule { }
