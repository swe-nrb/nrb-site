import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConceptRoutingModule } from './concept-routing.module';
import { ConceptListComponent } from './pages/concept-list/concept-list.component';
import { ConceptShowComponent } from './pages/concept-show/concept-show.component';
import { MaterialModule } from '../../material/material.module';
import { PartialsModule } from '../../partials/partials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { ClipboardModule } from 'ngx-clipboard';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ConceptRoutingModule,
    MaterialModule,
    PartialsModule,
    FormsModule,
    ReactiveFormsModule,
    MarkdownModule.forChild(),
    ClipboardModule,
    SharedModule
  ],
  declarations: [
    ConceptListComponent,
    ConceptShowComponent
  ]
})
export class ConceptModule { }
