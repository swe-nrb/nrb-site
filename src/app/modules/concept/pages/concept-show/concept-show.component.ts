import { Component, OnInit } from '@angular/core';
import { ConceptService } from '../../../../services/concept.service';
import { Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { switchMapTo, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-concept-show',
  templateUrl: './concept-show.component.html',
  styleUrls: ['./concept-show.component.css']
})
export class ConceptShowComponent implements OnInit {
  concept: any
  noResult: boolean = false;
  constructor(
    private conceptService: ConceptService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params
      .pipe(switchMap((params) => this.conceptService.getBySlug(params.conceptSlug)))
      .subscribe((concept) => {
        if(!concept) this.noResult = true;
        this.concept = concept;
      }, (err) => {
        console.log(err);
      });
  }
}
