import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptListComponent } from './concept-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ConceptService } from '../../../../services/concept.service';
import { MaterialModule } from '../../../../material/material.module';
import { PartialsModule } from '../../../../partials/partials.module';
import { ConceptCardComponent } from '../../components/concept-card/concept-card.component';
import { concepts } from '../../../../services/concepts.mock';
import { mockSources } from '../../../../services/sources.mock';

describe('ConceptListComponent', () => {
  let component: ConceptListComponent;
  let fixture: ComponentFixture<ConceptListComponent>;
  let service: ConceptService
  let httpMock: HttpTestingController

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MarkdownModule.forRoot(),
        ReactiveFormsModule,
        MaterialModule,
        PartialsModule,
        HttpClientTestingModule
      ],
      declarations: [ ConceptListComponent, ConceptCardComponent ],
      providers: [
        ConceptService
      ]
    })
    .compileComponents();

    service = TestBed.get(ConceptService);
    httpMock = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

    httpMock.match(`${service.url}/collection.json`);
    httpMock.match(`${service.url}/sources.json`);

    fixture.autoDetectChanges();
  });

  it('should load mock data', () => {
    var conceptRequest = httpMock.expectOne(`${service.url}/collection.json`);
    var sourceRequest = httpMock.expectOne(`${service.url}/sources.json`);
    
    conceptRequest.flush(concepts);
    sourceRequest.flush(mockSources);
  });
});
