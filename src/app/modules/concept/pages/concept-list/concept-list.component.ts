import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, merge, BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { startWith, map, switchMapTo, debounceTime } from 'rxjs/operators';
import * as _ from 'lodash';

import { ConceptService } from '../../../../services/concept.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-concept-list',
  templateUrl: './concept-list.component.html',
  styleUrls: ['./concept-list.component.css']
})
export class ConceptListComponent implements OnInit, OnDestroy {
  filterForm: FormGroup;
  sourceControl: FormControl;
  conceptSearchControl: FormControl;
  filterConceptsFunc: any
  languages: any[];
  tagStatuses: any[];
  tagNamePrePopulate: any;

  sources: any[];
  tags: Observable<any[]>;
  count: number;

  concepts$: Observable<any[]>;
  reload$: Observable<any>;

  urlUpdater: Subscription

  constructor(
    public conceptService: ConceptService,
    public route: ActivatedRoute,
    public location: Location
  ) {
    this.languages = [{
      value: 'sv',
      name: 'Svenska'
    }, {
      value: 'en',
      name: 'Engelska'
    }];
    this.tagNamePrePopulate = { short_name: 'Alla taggar', full_name: 'Alla taggar' };
    this.tagStatuses = [{
      value: 'all',
      name: 'Oavsett status'
    }, {
      value: 'idle',
      name: 'Ej påbörjad'
    }, {
      value: 'pending',
      name: 'Pågående'
    }, {
      value: 'complete',
      name: 'Fastställd'
    }];
    this.filterForm = new FormGroup({
      term: new FormControl(''),
      source: new FormControl(''),
      language: new FormControl(this.languages[0]),
      tagName: new FormControl(this.tagNamePrePopulate),
      tagStatus: new FormControl(this.tagStatuses[0]),
      limit: new FormControl(20)
    });
    
    this.count = 0;
    /**
     * We want to trigger a reload on the list based the following streams
     */
    this.reload$ = merge(
      this.filterForm.valueChanges
    );
    this.concepts$ = this.reload$
      .pipe(
        startWith([]),
        switchMapTo(this.conceptService.list()),
        map((concepts) => {
          let predicate: any = {};
          if(this.filterForm.get('source').value) predicate.source = this.filterForm.get('source').value;
          if(this.filterForm.get('language').value) predicate.language = this.filterForm.get('language').value.value;
          return _.filter(concepts, predicate);
        }),
        map(
          (concepts) => _.filter(concepts, (c) => {
            let haystack = c.term;
            if(c.abbrevation) haystack = haystack + " " + c.abbrevation;
            haystack = haystack.toLowerCase();
            let test = haystack.indexOf(this.filterForm.get('term').value.toLowerCase()) > -1;
            return test
          })
        ),
        map((concepts) => {
          if(this.filterForm.get('tagName').value.short_name == 'Alla taggar') {
            return concepts;
          } else {
            return _.filter(concepts, (concept) => {
              let predicate: any = { short_name: this.filterForm.get('tagName').value.short_name };
              if(this.filterForm.get('tagStatus').value.value != 'all') predicate.status = this.filterForm.get('tagStatus').value.value;
              let tag = _.find(concept.tags, predicate);
              return tag != undefined;
            });
          }
        }),
        map((concepts) => {
          this.count = concepts.length;
          return concepts;
        }),
        map((concepts) => _.sortBy(concepts, (o) => _.get(o, 'term', ''))),
        map((concepts) => concepts.splice(0,this.filterForm.get('limit').value))
      );

    this.tags = this.conceptService
      .getTags()
      .pipe(
        map((tags) => {
          this.filterForm.get('tagName').setValue(_.find(tags, (t) => t.short_name === 'Fastställd'));
          return tags;
        })
      );
    this.conceptService
      .getSources()
      .subscribe((sources) => this.sources = sources);
  }

  filteredSources: Observable<string[]>;

  ngOnInit() {
    this.runConfigFromUrl();
    
    this.filteredSources = this.filterForm.get('source').valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterFunc(val))
      );
    this.urlUpdater = this.filterForm.get('term').valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        (val) => {
          if(val) {
            this.location.replaceState('/concepts?term=' + val);
          } else {
            this.location.replaceState('/concepts');
          }
        }
      );
  }
  ngOnDestroy(): void {
    this.urlUpdater.unsubscribe();
  }

  filterFunc(val: string): string[] {
    if(this.sources) {
      return this.sources
        .filter(option => option.toLowerCase().indexOf(val.toLowerCase()) > -1);
    }
  }

  runConfigFromUrl(): void {
    if(this.route.snapshot.queryParamMap.get('term')) {
      this.filterForm.get('term').setValue(this.route.snapshot.queryParamMap.get('term'));
    }
    if(this.route.snapshot.queryParamMap.get('language')) {
      this.filterForm.get('language').setValue(_.find(this.languages, { value: this.route.snapshot.queryParamMap.get('language') }));
    }
  }
}

