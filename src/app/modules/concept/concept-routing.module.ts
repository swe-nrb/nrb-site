import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConceptListComponent } from './pages/concept-list/concept-list.component';
import { ConceptShowComponent } from './pages/concept-show/concept-show.component';

const routes: Routes = [{
  path: 'concepts',
  component: ConceptListComponent
}, {
  path: 'concepts/:conceptSlug',
  component: ConceptShowComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConceptRoutingModule { }
