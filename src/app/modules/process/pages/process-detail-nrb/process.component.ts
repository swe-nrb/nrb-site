import { Component, OnInit } from '@angular/core';
import { ProcessService } from '../../../../services/process.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {
  processes: Observable<any[]>;
  tags: any[]
  constructor(
    public processService: ProcessService
  ) {
    this.processes = this.processService.list();
  }

  ngOnInit() {
  }

}
