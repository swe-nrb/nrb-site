import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessComponent } from './process.component';
import { MaterialModule } from '../../../../material/material.module';
import { PartialsModule } from '../../../../partials/partials.module';
import { HttpClientModule } from '@angular/common/http';
import { ProcessService } from '../../../../services/process.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('ProcessComponent', () => {
  let component: ProcessComponent;
  let fixture: ComponentFixture<ProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        PartialsModule,
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [ ProcessComponent ],
      providers: [ProcessService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
