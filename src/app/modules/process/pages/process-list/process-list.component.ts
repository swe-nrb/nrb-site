import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-process-list',
  templateUrl: './process-list.component.html',
  styleUrls: ['./process-list.component.css']
})
export class ProcessListComponent implements OnInit {
  resources: any[]
  constructor() {
    this.resources = [
      {
        slug: 'nr',
        name: 'Livscykelprocess för byggd miljö',
        source: 'Nationella riktlinjer',
        language: 'sv',
        definition: 'Livscykelprocessen består av ett antal skeden och aktiviteter. En kod och syften är definierade för varje aktivitet.',
        comment: 'Livscykelprocessens indelning och aktivitetsbenämning bygger på en partiell översättning av SS-EN 16310 2013',
        tags: [
          {
            "tag": "nrb-tag-definition-nrb",
            "concept": "byggnadsinformationsmodellering-bim-isodis-19650-122018",
            "short_name": "Fastställd",
            "full_name": "Fastställd som en nationell riktlinje",
            "description": "Indikerar att begreppet är kvalificerat som ett fastställt enligt steg 3 i förvaltningsstadgarna för nationella riktlinjers begreppslista",
            "review_steps": [
              {
                "name": "Kandidat",
                "description": "Indikerar att begreppet är en kandidat till att prövas som ett rekommenderat begrepp",
                "complete": true,
                "author": {
                  "name": "Damir Omerovic",
                  "linkedin": "https://www.linkedin.com/in/damir-omerovic-45117113a/"
                },
                "date": "2020-02-27"
              },
              {
                "name": "Kvalitetssäkrad",
                "description": "Begreppet kan endast passera detta steg om begreppet redan har taggen \"Kvalitetsgranskad\"",
                "complete": true,
                "author": {
                  "name": "Damir Omerovic",
                  "linkedin": "https://www.linkedin.com/in/damir-omerovic-45117113a/"
                },
                "date": "2020-02-27"
              },
              {
                "name": "Fastställd",
                "description": "Beslut fattas av förvaltningsgruppen. Ev. motivering skrivs här",
                "complete": true,
                "author": {
                  "name": "Damir Omerovic",
                  "linkedin": "https://www.linkedin.com/in/damir-omerovic-45117113a/"
                },
                "date": "2020-02-27"
              }
            ],
            "review_length": 3,
            "status": "complete",
            "currentStep": {
              "name": "Fastställd",
              "description": "Beslut fattas av förvaltningsgruppen. Ev. motivering skrivs här",
              "complete": true,
              "author": {
                "name": "Damir Omerovic",
                "linkedin": "https://www.linkedin.com/in/damir-omerovic-45117113a/"
              },
              "date": "2020-02-27"
            },
            "fullfilledCount": 3,
            "percentDone": 1
          },
          {
            "tag": "nrb-tag-definition-quality",
            "concept": "byggnadsinformationsmodellering-bim-isodis-19650-122018",
            "short_name": "Kvalitetsgranskad",
            "full_name": "Kvalitetsgranskad",
            "description": "Begreppet ska uppfylla de kvalitetskrav som är uppsatta i stadgarna",
            "review_steps": [
              {
                "name": "Granskad",
                "description": "Begreppet är kvalitetsgranskat",
                "complete": true,
                "author": {
                  "name": "Damir Omerovic",
                  "linkedin": "https://www.linkedin.com/in/damir-omerovic-45117113a/"
                },
                "date": "2020-02-27"
              }
            ],
            "review_length": 1,
            "status": "complete",
            "currentStep": {
              "name": "Granskad",
              "description": "Begreppet är kvalitetsgranskat",
              "complete": true,
              "author": {
                "name": "Damir Omerovic",
                "linkedin": "https://www.linkedin.com/in/damir-omerovic-45117113a/"
              },
              "date": "2020-02-27"
            },
            "fullfilledCount": 1,
            "percentDone": 1
          }
        ]
      }
    ];
  }

  ngOnInit() {
  }

}
