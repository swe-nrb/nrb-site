import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessComponent } from './pages/process-detail-nrb/process.component';
import { ProcessListComponent } from './pages/process-list/process-list.component';

const routes: Routes = [{
  path: 'processes/nr',
  component: ProcessComponent
}, {
  path: 'processes',
  component: ProcessListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessRoutingModule { }
