import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProcessRoutingModule } from './process-routing.module';
import { MaterialModule } from '../../material/material.module';
import { PartialsModule } from '../../partials/partials.module';
import { MarkdownModule } from 'ngx-markdown';
import { SharedModule } from '../shared/shared.module';

import { ProcessComponent } from './pages/process-detail-nrb/process.component';
import { ProcessListComponent } from './pages/process-list/process-list.component';

@NgModule({
  imports: [
    CommonModule,
    ProcessRoutingModule,
    MaterialModule,
    SharedModule,
    PartialsModule,
    MarkdownModule
  ],
  declarations: [ProcessComponent, ProcessListComponent]
})
export class ProcessModule { }
