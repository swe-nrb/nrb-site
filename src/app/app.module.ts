import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { PageModule } from './modules/page/page.module';
import { ProcessModule } from './modules/process/process.module';
import { MethodModule } from './modules/method/method.module';
import { DeliverySpecificationModule } from './modules/delivery-specification/delivery-specification.module';
import { MaterialModule } from './material/material.module';

import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
import { SidenavComponent } from './partials/sidenav/sidenav.component';

import { ConceptService } from './services/concept.service';
import { ProcessService } from './services/process.service';
import { PartialsModule } from './partials/partials.module';
import { SidenavService } from './services/sidenav.service';
import { MethodService } from './services/method.service';
import { ConceptModule } from './modules/concept/concept.module';
import { LibraryModule } from './modules/library/library.module';
import { NotFoundModule } from './modules/not-found/not-found.module';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './modules/shared/shared.module';
import { ReferenceService } from './services/reference.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    PartialsModule,
    PageModule,
    LibraryModule,
    ConceptModule,
    MethodModule,
    ProcessModule,
    DeliverySpecificationModule,
    SharedModule,
    NotFoundModule // import last
  ],
  providers: [
    ConceptService,
    ProcessService,
    SidenavService,
    MethodService,
    ReferenceService
  ],
  exports: [
    MaterialModule,
    PartialsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
