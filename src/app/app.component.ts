import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidenavService } from './services/sidenav.service';
import { Observable } from 'rxjs';
import { IconsService } from './material/icons.service';
import { Router, NavigationEnd, Event } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    public sidenavService: SidenavService,
    public iconService: IconsService,
    public router: Router
  ) { }
  ngOnInit() {
    this.router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {
        window['gtag']('config', 'UA-72726856-6', {
          'page_title' : event.urlAfterRedirects,
          'page_path': event.urlAfterRedirects
        });
      }
    });

  }
}
