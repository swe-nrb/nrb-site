import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfopackFileBrowserComponent } from './infopack-file-browser/infopack-file-browser.component';
import { MaterialModule } from '../material/material.module';
import { InfopackFileBrowserModalComponent } from './infopack-file-browser/modal/modal.component';
import { ReadMoreComponent } from './read-more/read-more.component';
import { MarkdownModule } from 'ngx-markdown';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MarkdownModule.forChild()
  ],
  declarations: [
    InfopackFileBrowserComponent,
    InfopackFileBrowserModalComponent,
    ReadMoreComponent
  ],
  exports: [InfopackFileBrowserComponent, ReadMoreComponent],
  entryComponents: [InfopackFileBrowserModalComponent]
})
export class PartialsModule { }
