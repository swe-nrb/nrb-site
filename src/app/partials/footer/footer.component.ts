import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  contactOpen: Boolean
  aboutOpen: Boolean
  contributeOpen: Boolean
  constructor() {
    this.contactOpen = false;
    this.aboutOpen = false;
    this.contributeOpen = false;
  }

  ngOnInit() {
  }

}
