import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { MarkdownComponent } from 'ngx-markdown';

@Component({
  selector: 'app-read-more',
  templateUrl: './read-more.component.html',
  styleUrls: ['./read-more.component.css']
})
export class ReadMoreComponent implements OnInit {
  @Input('data') data: string
  constructor() { }

  ngOnInit() { }

  seeMore(event) {
    event.target.style.display = "none";
    event.target.parentNode.style.height = "100%";
    event.target.parentNode.style['max-height'] = "100%";
  }

}
