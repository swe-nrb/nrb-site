export interface PageLink {
    name: String
    icon: String
    url?: String
    children?: PageLinkChildren[]
}

export interface PageLinkChildren {
    name: String
    url: String
    queryParams?: any
    isExternal?: Boolean
}
