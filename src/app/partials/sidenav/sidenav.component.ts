import { Component, OnInit } from '@angular/core';
import { SidenavService } from '../../services/sidenav.service';
import { PageLink } from './sidenav-page-link';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  pages: PageLink[];
  showSubmenu: boolean[];
  constructor(
    private sidenavService: SidenavService,
    private router: Router
  ) {
    this.pages = [{
      url: '/introduction/about',
      name: 'Introduktion',
      icon: 'nrb:compass',
      children: [
        {
          url: '/introduction/about',
          name: 'Om riktlinjerna',
        },
        {
          url: '/introduction/management',
          name: 'Förvaltning',
        },
        {
          url: '/introduction/applications',
          name: 'Så använder du riktlinjerna',
        }
      ]
    }, {
      url: '/guidelines',
      name: 'Fastställda riktlinjer',
      icon: 'nrb:direction',
      children: [
        {
          url: '/guidelines/requirements',
          name: 'Informationskrav',
        },
        {
          url: '/guidelines/information-models',
          name: 'Informationsmodeller',
        },
        {
          url: '/guidelines/processes',
          name: 'Processer',
        },
        {
          url: '/guidelines/organization',
          name: 'Organisation',
        }
      ]
    }, {
      url: '/library',
      name: 'Referenser',
      icon: 'nrb:library'
    }, {
      name: 'Grunder',
      icon: 'nrb:graph',
      children: [
        {
          url: '/concepts',
          name: 'Begrepp',
        },
        {
          url: '/methods',
          name: 'Metoder',
        },
        {
          url: '/processes',
          name: 'Processer',
        },
        {
          url: '/delivery-specifications',
          name: 'Leveransspecifikationer',
        }
      ]
    }, {
      name: 'Information',
      icon: 'nrb:info',
      children: [
        {
          url: '/contact',
          name: 'Kontakt',
        },
        {
          url: 'https://gitter.im/swe-nrbg/Lobby',
          name: 'Diskussionsforum',
          isExternal: true
        },
        {
          url: 'https://bimalliance.se/',
          name: 'BIM Alliance',
          isExternal: true
        }
      ]
    }];

    this.showSubmenu=[];
  }

  ngOnInit() { }

  onNavigate(page?, event?) {
    if(this.sidenavService.sidenav.mode == 'over') {
      this.sidenavService.close();
    }
    this.router.navigate(['/']);
  }
}
