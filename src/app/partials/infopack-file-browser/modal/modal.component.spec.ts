import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfopackFileBrowserModalComponent } from './modal.component';
import { MaterialModule } from '../../../material/material.module';

describe('ModalComponent', () => {
  let component: InfopackFileBrowserModalComponent;
  let fixture: ComponentFixture<InfopackFileBrowserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule
      ],
      declarations: [ InfopackFileBrowserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfopackFileBrowserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
