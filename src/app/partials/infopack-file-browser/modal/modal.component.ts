import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class InfopackFileBrowserModalComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public input: { fileList: Observable<any[]>, url: string }
  ) { }

  ngOnInit() { }

}
