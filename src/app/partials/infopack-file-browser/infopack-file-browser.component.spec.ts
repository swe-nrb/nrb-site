import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfopackFileBrowserComponent } from './infopack-file-browser.component';
import { MaterialModule } from '../../material/material.module';
import { PartialsModule } from '../partials.module';

describe('InfopackFileBrowserComponent', () => {
  let component: InfopackFileBrowserComponent;
  let fixture: ComponentFixture<InfopackFileBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        PartialsModule
      ],
      declarations: [ ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfopackFileBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
