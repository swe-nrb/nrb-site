import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { InfopackFileBrowserModalComponent } from './modal/modal.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-infopack-file-browser',
  templateUrl: './infopack-file-browser.component.html',
  styleUrls: ['./infopack-file-browser.component.css']
})
export class InfopackFileBrowserComponent implements OnInit {
  @Input('infopack-url') infopackUrl: string
  metadata: any
  constructor(
    public dialog: MatDialog,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.http
      .get(`${this.infopackUrl}/assets/infopack.json`)
      .subscribe((metadata) => this.metadata = metadata)
  }

  openDialog() {
    let dialogRef = this.dialog.open(InfopackFileBrowserModalComponent, {
      data: { fileList: this.http.get(`${this.infopackUrl}/assets/output.json`), url: this.infopackUrl }
    });

  }

}
