import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

@Injectable()
export class IconsService {

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    console.log('LOADING NRB ICONS');
    iconRegistry.addSvgIconInNamespace('nrb', 'home', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-home.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'arrow-right', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-arrow-right.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'bell', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-bell.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'compass', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-compass.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'home', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-home.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'info', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-info.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'like', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-like.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'mail', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-mail.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'megafon', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-megafon.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'share', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-share.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'source', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-source.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'process', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-process.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'star', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-star.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'download', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-download.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'search', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-search.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'heart', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-heart.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'layers', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-layers.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'lifecycle', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-lifecycle.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'tag-avrader', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-tag-avrader.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'tag-granskning', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-tag-granskning.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'tag-information', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-tag-information.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'tag-kandidat', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-tag-kandidat.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'direction', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-direction.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'library', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-library.svg'));
    iconRegistry.addSvgIconInNamespace('nrb', 'graph', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/nrb-graph.svg'));
  }

}
