import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatButtonModule,
  MatSidenavModule,
  MatDialogModule,
  MatListModule,
  MatIconModule,
  MatDividerModule,
  MatChipsModule,
  MatToolbarModule,
  MatInputModule,
  MatAutocompleteModule,
  MatMenuModule,
  MatSnackBarModule,
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { IconsService } from './icons.service';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatDialogModule,
    MatListModule,
    MatIconModule,
    MatDividerModule,
    MatChipsModule,
    MatToolbarModule,
    LayoutModule,
    MatInputModule,
    MatAutocompleteModule,
    MatMenuModule,
    MatSnackBarModule
  ],
  exports: [
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatDialogModule,
    MatListModule,
    MatIconModule,
    MatDividerModule,
    MatChipsModule,
    MatToolbarModule,
    LayoutModule,
    MatInputModule,
    MatAutocompleteModule,
    MatMenuModule,
    MatSnackBarModule
  ],
  providers: [IconsService],
  declarations: []
})
export class MaterialModule { }
