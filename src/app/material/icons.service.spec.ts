import { TestBed, inject } from '@angular/core/testing';

import { IconsService } from './icons.service';
import { MatIconRegistry } from '@angular/material';

describe('IconsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IconsService, MatIconRegistry]
    });
  });

  it('should be created', inject([IconsService], (service: IconsService) => {
    expect(service).toBeTruthy();
  }));
});
